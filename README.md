Leap
====

Leap motion face recognition utility

LeapJava.jar (from the leap motion sdk) had to be installed in your local maven repository.
use:
mvn org.apache.maven.plugins:maven-install-plugin:2.3.1:install-file \
    -Dfile=PATH_TO_JAR \
    -DgroupId=com.leapmotion.leap -DartifactId=leapMotion \
    -Dversion=X.X.X -Dpackaging=jar


to build:
mvn package

