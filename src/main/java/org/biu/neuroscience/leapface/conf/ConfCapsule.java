package org.biu.neuroscience.leapface.conf;

import java.io.Serializable;

/* Encapsulation class for the configurations read form conf.ini
 * Since the files saved in the app depend on the configurations
 * in the time they were created, they must keep a copy of
 * that conf with them. Once a face is created a conf capsule
 * is attached to it, and any flow from that point will use the configuration
 * capsule that is found in the face */
public class ConfCapsule implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4785110365477278405L;
	//[face]
	int ySpan; //the length (in leap points which are mm) between the glass platform and the highest point on the face
	String faceParts; //comma separated string of the names of face parts
	/* when we use the pen in the calibration panel to define a face part by a curve
	 * we treat the points it comprises as polygon vertices. If the curve is very dense we don't really
	 * need all the vertices so we thin them out. this is the minial distance (in pixels) between two adjecant vertices
	 */
	int minimalVerticesDistance; 
	//[workflow]
	/* A pretty useless configuration. if set to true a prompt will open in the calibration panel
	 * when rec is hit to let you prepare your hand on the face before it starts recording	 */
	boolean dialogOnRec;
	//how many characters will be visible in the experiment panel console
	int consoleCharLim;
	//[leap]
	//one if how many frames the leap controller gets will update display observers
	int displayUpdateCycle;
	/*each time we want to record something with the controller we check that it's connected.
	 * we to a sleep-try loop up to a timeout to see if the controller is connected after the controller object
	 * is created. we set the timeout here (here in the conf file that is) in millis */
	int leapConnectTimeout;

	//[processing]
	/* if a finger (by id) appeared for less than this value (in microsecond) we delete it as it is
	 * probably an artifact	 */
	int minimalFingerPersistence;
	
	
	
	public int getYSpan() {
		return ySpan;
	}
	public void setYSpan(int ySpan) {
		this.ySpan = ySpan;
	}
	public String getFaceParts() {
		return faceParts;
	}
	public void setFaceParts(String faceParts) {
		this.faceParts = faceParts;
	}
	public int getMinimalVerticesDistance() {
		return minimalVerticesDistance;
	}
	public void setMinimalVerticesDistance(int minimalVerticesDistance) {
		this.minimalVerticesDistance = minimalVerticesDistance;
	}

	public void setDialogOnRec(boolean dialogOnRec) {
		this.dialogOnRec = dialogOnRec;
	}
	public boolean isDialogOnRec() {
		return dialogOnRec;
	}
	public int getConsoleCharLim() {
		return consoleCharLim;
	}
	public void setConsoleCharLim(int consoleCharLim) {
		this.consoleCharLim = consoleCharLim;
	}
	public int getDisplayUpdateCycle() {
		return displayUpdateCycle;
	}
	public void setDisplayUpdateCycle(int displayUpdateCycle) {
		this.displayUpdateCycle = displayUpdateCycle;
	}
	public int getMinimalFingerPersistence() {
		return minimalFingerPersistence;
	}
	public void setMinimalFingerPersistence(int minimalFingerPersistence) {
		this.minimalFingerPersistence = minimalFingerPersistence;
	}

	public int getLeapConnectTimeout() {
		return leapConnectTimeout;
	}
	public void setLeapConnectTimeout(int leapConnectTimeout) {
		this.leapConnectTimeout = leapConnectTimeout;
	}
	
}


