package org.biu.neuroscience.leapface.experiment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.biu.neuroscience.leapface.experiment.face.Face;

/* Facade class for com.leapmotion.leap.Frame,
 * comprising less data and functionality.
 * We only keep a timestamp and a list of FFingers */
public class FFrame implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8123516742027873908L;
	private long timestamp;
	List<FFinger> fingers = new ArrayList<FFinger>();
	
	public long getTimestamp() {
		return timestamp;
	}
	
	public void setTimestamp(long timestamp) {
		this.timestamp=timestamp;
	}
	
	public void addFinger(FFinger finger) {
		fingers.add(finger);
	}

	public List<FFinger> getFingers() {
		return fingers;
		
	}
	
	public boolean isEmpty() {
		return fingers.isEmpty();
		
	}

	
	//returns true if there is a finger in the frame that is on one of the parts in face
	public boolean isThereFingerOnFacePart(Face face) {
		for (FFinger finger : fingers)
			if (!face.whereIs(finger.getTip()).isEmpty())
				return true;
		return false;
	}
	
	@Override
	public String toString() {
		String returnS = "FFrame [timestamp=" + timestamp + ", fingers:\n ";
		for (FFinger finger : fingers)
			returnS+=finger;
		return returnS+ "]"; 
		
	}
	
	public void removeFingerById(Integer id) {
		for (int i=0; i<fingers.size();i++)
			if (fingers.get(i).getId().equals(id))
				fingers.remove(i);
	}
	
}
