package org.biu.neuroscience.leapface.experiment;

import java.io.Serializable;

/* a simple 2D Vector */
public class Point2D implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5471373039497965417L;
	private Float x;
	private Float z;
	public Point2D(Float x, Float z)  {
		super();
		this.x = x;
		this.z = z;
	}
	public Float getX() {
		return x;
	}
	public void setX(Float x) {
		this.x = x;
	}
	public Float getZ() {
		return z;
	}
	public void setZ(Float z) {
		this.z = z;
	}
	
	
}
