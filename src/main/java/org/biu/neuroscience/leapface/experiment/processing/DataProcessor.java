package org.biu.neuroscience.leapface.experiment.processing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.Range;
import org.apache.log4j.Logger;
import org.biu.neuroscience.leapface.conf.ConfCapsule;
import org.biu.neuroscience.leapface.experiment.FFinger;
import org.biu.neuroscience.leapface.experiment.FFrame;
import org.biu.neuroscience.leapface.experiment.face.Face;

/* This class crunches the data.
 * To create an instance a conf capsule has to be provided */
public class DataProcessor {

	public static final Logger LOGGER = Logger.getLogger(DataProcessor.class);

	// in microseconds, the unit leap motion uses
	private int minimalFingerPersistence;
	private int numOfFaceParts;
	
	private ConfCapsule conf;
	
	
	
	public DataProcessor(ConfCapsule conf) {
		this.conf=conf;
		// get minimalFingerPersistence and numOfFaceParts from configuartion

		minimalFingerPersistence = this.conf.getMinimalFingerPersistence();
		numOfFaceParts = this.conf.getFaceParts().split(",").length;

	}
	
	 /* this method removes noise from the data. Any finger that appears for less
	 * than the minimum finger persistence time is removed.
	 * The method receives a list of tapes and returns one combined, clean tape	 */
	public List<FFrame> refineData(List<List<FFrame>> tapes) {

		List<FFrame> returnTape = new ArrayList<FFrame>();


		
		for (List<FFrame> tape : tapes)
			returnTape.addAll(tape);
		
	
		Map<Integer, Integer> fingerAppearances = new HashMap<Integer, Integer>();
		
		for (int i = 0; i < returnTape.size(); i++) {
			List<Integer> frameFingerIds = new ArrayList<Integer>();
			for (FFinger finger : returnTape.get(i).getFingers())
				frameFingerIds.add(finger.getId());

			// add the fingers that appear just now to the map
			for (Integer id : frameFingerIds) {
				if (fingerAppearances.get(id) == null) {
					fingerAppearances.put(id, i);
				}
			}
			// check for each finger in the map if it is still here, if it isn't
			// check if it persisted long enough
			for (Map.Entry<Integer, Integer> finFirstApp : (new HashMap<Integer,Integer>(fingerAppearances)).entrySet()) {

				// there is a finger that's still in the map but isn't in the
				// frame
				if (!frameFingerIds.contains(finFirstApp.getKey())) {
					// was the finger here long enough?
					long lastTimeStamp = returnTape.get(i - 1).getTimestamp();
					long firstTimeStamp = returnTape
							.get(finFirstApp.getValue()).getTimestamp();
					// it wasn't
					if (lastTimeStamp - firstTimeStamp < minimalFingerPersistence) {
						// go through all the frame from the time it first
						// appeared to this frame and remove this finger
						for (int j = finFirstApp.getValue(); j < i; j++)
							returnTape.get(j).removeFingerById(
									finFirstApp.getKey());
					}
					// it was. let's remove it from the map and forget about it
					else {
						fingerAppearances.remove(finFirstApp.getKey());
					}
				}
			}
		}


		return returnTape;
	}
	
	
	/* this method translates the timestamps so that each timestamp=timestamp+offset */
	public List<FFrame> shiftTimestamps(List<FFrame> tape, long offset) {
		List<FFrame> returnTape = new ArrayList<FFrame>(tape);
		for (FFrame frame : returnTape)
			frame.setTimestamp(frame.getTimestamp()+offset);
		
		return returnTape;
		
	}
	
	/* creates analyzed blocks from the tape, see AnalyzedBlcok	 */
	public List<AnalyzedBlock> analyzeData(List<FFrame> tape, Face face) {
		
		List<AnalyzedBlock> returnList = new ArrayList<AnalyzedBlock>();
		
		long blockStart=0;
		Map<Integer, Integer> currentPartNumsToNumOfFingers = new HashMap<Integer,Integer>();
		int blockIndex=0;
		
		for (FFrame frame : tape) {		
			
			
			//count how many fingers on each part in this frame;.
			List<List<Integer>> listOfPartsForFingerLists = new ArrayList<List<Integer>>();			
			for (FFinger finger : frame.getFingers()) {
				listOfPartsForFingerLists.add(face.whereIs(finger.getTip()));
			}
			int[] partsFingerCount=new int[numOfFaceParts];
			for (List<Integer> fingerParts : listOfPartsForFingerLists)
				for (Integer part : fingerParts)
					partsFingerCount[part]++;
			
			
			boolean frameIsDifferent=false;
			
			
			//check for a descrepancy between the current count of fingers per part and what is currently in the map
			for (Map.Entry<Integer, Integer> partFingers : currentPartNumsToNumOfFingers.entrySet()) 
				if (partsFingerCount[partFingers.getKey()]!=partFingers.getValue())
					frameIsDifferent=true;
			
			//the last map is empty and the frame isn't
			if (frame.isThereFingerOnFacePart(face) && currentPartNumsToNumOfFingers.isEmpty())
				frameIsDifferent=true;
				

			
			
			/* if frame is different or it is the last we close the block 
			 * start a new current map and save the previous one as an analysed block (which is
			 * irrelevant if it is the last frame)	 */
			if (frameIsDifferent || frame.getTimestamp()==tape.get(tape.size()-1).getTimestamp()) {
				AnalyzedBlock newBlock=new AnalyzedBlock();
				newBlock.setTimeRange(Range.between(blockStart, frame.getTimestamp()-1));
				newBlock.setPartToFingerAmountOnIt(currentPartNumsToNumOfFingers);
				newBlock.setIndex(blockIndex++);
				returnList.add(newBlock);
				blockStart=frame.getTimestamp();
				//create a new map base on the current frame for the next block
				currentPartNumsToNumOfFingers=new HashMap<Integer,Integer>();
				for (int i=0; i< partsFingerCount.length;i++)
					if (partsFingerCount[i]!=0)
						currentPartNumsToNumOfFingers.put(i, partsFingerCount[i]);
				
				

			}
	
		}

		return returnList;
	}
	

	public long totalTimeSpentInPart(ExpResult result, int partNum) {
		long total = 0;	
		
		for (AnalyzedBlock block : result.getAnalyzedData())  {
			if (block.getPartToFingerAmountOnItMap().get(partNum)!=null
			&& block.getPartToFingerAmountOnItMap().get(partNum)>0)
				total+=block.getTimespan();	
		}

		return total;	
	}
	
	/* returns a list of blocks in which at least one finger is in
	 * the given part	 */
	public static List<AnalyzedBlock> partRanges(ExpResult result, int partNum) {
		List<AnalyzedBlock> returnList = new ArrayList<AnalyzedBlock>();
		for (AnalyzedBlock block : result.getAnalyzedData())  {
			if (block.getPartToFingerAmountOnItMap().get(partNum)!=null
			&& block.getPartToFingerAmountOnItMap().get(partNum)>0)
				returnList.add(block);	
		}
		
		return returnList;
	
	}

	/* returns all the blocks within a time range	 */
	public List<AnalyzedBlock> rangeParts(ExpResult result, Range<Long> range) {
		List<AnalyzedBlock> returnList = new ArrayList<AnalyzedBlock>();
		for (AnalyzedBlock block : result.getAnalyzedData())  {
			if (range.containsRange(block.getTimeRange()))
				returnList.add(block);	
		}
		return returnList;
	}
}
