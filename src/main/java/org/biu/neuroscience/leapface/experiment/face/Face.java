package org.biu.neuroscience.leapface.experiment.face;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.Range;
import org.biu.neuroscience.leapface.conf.ConfCapsule;
import org.biu.neuroscience.leapface.experiment.Point;
import org.biu.neuroscience.leapface.experiment.Point2D;

/* A face, as calibrated in the calibration interface.
 * Contains a map from face part number to a bounary object for that part
 * A range between the lowest possible point and the highest possible point (in leap y coordinates)
 * A configuration capsule, containing the configurations at the time the face was created.
 * Since this object can be written to a file and it depends on the apps configuration it has
 * to have these configurations attached.
 * 
 */
public class Face implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5713185206303429454L;

	private Map<Integer,Boundary> parts;
	private Range<Float> yRange;
	private ConfCapsule conf;
	
	public Face(ConfCapsule conf) {
		super();
		this.conf=conf;
		parts = new HashMap<Integer,Boundary>();
	}
	
	public boolean isEmpty() {
		return parts.isEmpty();
	}
	
	public ConfCapsule getConf() {
		return conf;
	}

	public void setConf(ConfCapsule conf) {
		this.conf = conf;
	}

	public void setYRange(Float end1, Float end2) {
		yRange=Range.between(end1,end2);
	}
	
	public void addPart(Integer partNum, Boundary boundary) {
		parts.put(partNum, boundary);
	}
	
	public void removePart(Integer partNum) {
		parts.remove(partNum);
	}
	
	public List<Integer> whereIs(Point point) {
		
		List<Integer> partsPointIsIn = new ArrayList<Integer>();
		if (!yRange.contains(point.getY()))
			return partsPointIsIn;
		
		Point2D projection = new Point2D(point.getX(),point.getZ());
		
		for (Map.Entry<Integer,Boundary > part : parts.entrySet()) 
			if (part.getValue().isInBoundary(projection))
				partsPointIsIn.add(part.getKey());	
		return partsPointIsIn;
	}
	
	
	
	
	
}
