package org.biu.neuroscience.leapface.experiment.processing;

import java.io.Serializable;
import java.util.List;

import org.biu.neuroscience.leapface.conf.ConfCapsule;
import org.biu.neuroscience.leapface.experiment.FFrame;
import org.biu.neuroscience.leapface.experiment.face.Face;

/* A class that stores the result of an experiment.
 * contains the analyzed data (a list of analyzed blocks),
 * the refined data (see DataProcessor.refineData(...)
 * the raw data of the experiment, and the face. */
public class ExpResult implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6473017488655280837L;
	List<AnalyzedBlock> analyzedData;
	List<FFrame> refinedData;
	List<List<FFrame>> rawData;
	Face face;

	public List<AnalyzedBlock> getAnalyzedData() {
		return analyzedData;
	}
	public void setAnalyzedData(List<AnalyzedBlock> analyzedData) {
		this.analyzedData = analyzedData;
	}
	public List<FFrame> getRefinedData() {
		return refinedData;
	}
	public void setRefinedData(List<FFrame> refinedData) {
		this.refinedData = refinedData;
	}
	public List<List<FFrame>> getRawData() {
		return rawData;
	}
	public void setRawData(List<List<FFrame>> rawData) {
		this.rawData = rawData;
	}
	public Face getFace() {
		return face;
	}
	public void setFace(Face face) {
		this.face = face;
	}
	
	
	public int expLenghInSeconds() {
		return (int) refinedData.get(refinedData.size()-1).getTimestamp() / 1000000;
	}
	public ConfCapsule getConf() {
		return face.getConf();
	}

	
}
