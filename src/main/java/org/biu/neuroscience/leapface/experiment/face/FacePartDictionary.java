package org.biu.neuroscience.leapface.experiment.face;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.biu.neuroscience.leapface.conf.ConfCapsule;

import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;


/* bidirectional translator for face parts and numbers
 * To create a new instance a configuration capsule has to be fed to the
 * constructor
 */
public class FacePartDictionary {
	

	private BiMap<Integer,String> facePartsMap;
	 
	public FacePartDictionary(ConfCapsule conf) {
		
		//get face parts from configuartion			
		
			String raw = conf.getFaceParts();
			Integer partNumber=0;
			ImmutableBiMap.Builder<Integer,String> builder = new ImmutableBiMap.Builder<Integer,String>();
			for (String partName : raw.split(",")) {
				builder.put(partNumber, partName );
				partNumber++;
			}
			facePartsMap = builder.build();

	}
	
	//get a list of all parts
	public  List<String> getParts() {
		return new ArrayList<String>(facePartsMap.values());	
	}
	
	
	//get part number for a part
	public Integer getPartNumber(String partName) {
		return facePartsMap.inverse().get(partName);
	}
	
	
	//get part name for a number
	public String getPartName(Integer partNumber) {
		return facePartsMap.get(partNumber);
	}
	
	public Map<Integer,String> getMap() {
		return new HashMap<Integer,String>(facePartsMap);
	}
	
		
		
}
