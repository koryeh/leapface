package org.biu.neuroscience.leapface.experiment;

import java.util.ArrayList;
import java.util.List;
import java.util.Observer;

import org.biu.neuroscience.leapface.conf.ConfCapsule;
import org.biu.neuroscience.leapface.experiment.face.Face;
import org.biu.neuroscience.leapface.experiment.processing.DataProcessor;
import org.biu.neuroscience.leapface.leap.LeapAdapter;


/* The class responsible for the experiment process
 * it's data is stored as an ExpData object.
 * it has a leap adapter, and is in one of two states,
 * running and not running.
 * each time stop is called we take the tape from the adapter
 * and add it to expData's tapes. when we are finished running the
 * experiment glueTapes should be called to make the tapes contiguous
 * as explained in ExpData */
public class ExpRunner {

	private ExpData expData;
	private LeapAdapter adapter;
	
	private int state=NOT_RUNNING;
	private static final int NOT_RUNNING=0;
	private static final int RUNNING=1;
	

	
	public ExpRunner(Face face) {
		expData=new ExpData();
		expData.setFace(face);
		expData.setTapes(new ArrayList<List<FFrame>>());
		adapter = new LeapAdapter(getConf());
	}
	
	public ExpRunner(ExpData expData) {
		this.expData=expData;
		adapter = new LeapAdapter(getConf());
	}
	
	public void start(Observer display) {
		if (state==RUNNING)
			return;
		state=RUNNING;
		adapter.rec(display,LeapAdapter.RAW_FFRAME_DISPLAY);
	}
	
	public void stop() {
		if (state==NOT_RUNNING)
			return;
		state=NOT_RUNNING;
		expData.getTapes().add(adapter.stop());
		glueTapes();
	}
	


	public Face getFace() {
		return expData.getFace();
	}
	
	public ExpData getExpData() {
		return expData;
		
	}

	public ConfCapsule getConf() {
		return expData.getConf();
	}
	
	public void glueTapes() {
		DataProcessor processor = new DataProcessor(expData.getFace().getConf());
		long lastTipeEndTimestamp=0;		
		for (List<FFrame> tape : expData.getTapes()) {
			//start tape from zero
			processor.shiftTimestamps(tape,-tape.get(0).getTimestamp());
			//start tape from last tape end
			processor.shiftTimestamps(tape, lastTipeEndTimestamp+1);		
			lastTipeEndTimestamp=tape.get(tape.size()-1).getTimestamp();
		}
	}

}
