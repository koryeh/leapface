package org.biu.neuroscience.leapface.experiment.face;

import java.io.Serializable;
import java.util.List;

import org.biu.neuroscience.leapface.experiment.Point2D;


/* A boundary for a face part.
 * this class consists of a polygon given by it's vertices,
 * and the coordinates for the rectangle bounding this polygon.
 * A boundary object can tell if a point is inside of it using the
 * isInBoundary method */
public class Boundary implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8216183768864222548L;
	
	
	List<Point2D> vertices;
	float minX;
	float maxX;
	float minZ;
	float maxZ;
	
	
	
	public Boundary(List<Point2D> vertices) {
		super();
		this.vertices = vertices;
		minX=Float.POSITIVE_INFINITY;
		minZ=Float.POSITIVE_INFINITY;
		maxX=Float.NEGATIVE_INFINITY;
		maxZ=Float.NEGATIVE_INFINITY;
		for (Point2D point : vertices) {
			float x = point.getX();
			float z = point.getZ();
			if (x > maxX)
				maxX=x;
			if (x < minX)
				minX=x;
			if (z>maxZ)
				maxZ=z;
			if (z<minZ)
				minZ=z;
		}

	}



	/* see http://www.codeproject.com/Tips/84226/Is-a-Point-inside-a-Polygon
	 * for reference  */
	public boolean isInBoundary(Point2D p) {

		float x = p.getX();
		float z = p.getZ();
		
		//check if the point is inside to boundary's bounding rectangle
		if (x>maxX || x<minX || z>maxZ ||z<minZ)
			return false;
		int verticesNum = vertices.size();
		boolean rValue=false;
		int i,j;
		  for (i = 0,  j = verticesNum-1; i < verticesNum; j = i++) {
			    if ( ((vertices.get(i).getZ()>z) != (vertices.get(j).getZ()>z)) &&
				 (x < (vertices.get(j).getX()-vertices.get(i).getX() * (z-vertices.get(i).getZ()) / (vertices.get(j).getZ()-vertices.get(i).getZ()) + vertices.get(i).getX())))
			    	rValue = !rValue;
			  }
		return rValue;
		
	}
	

	
	
	
}
