package org.biu.neuroscience.leapface.experiment;

import java.io.Serializable;

/* a simple 3D Vector */
public class Point implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -320095557907572463L;
	private Float x;
	private Float y;
	private Float z;
	public Point(Float x, Float y, Float z) {
		super();
		this.x = x;
		this.y = y;
		this.z = z;
	}
	public Float getX() {
		return x;
	}
	public void setX(Float x) {
		this.x = x;
	}
	public Float getY() {
		return y;
	}
	public void setY(Float y) {
		this.y = y;
	}
	public Float getZ() {
		return z;
	}
	public void setZ(Float z) {
		this.z = z;
	}
	

}
