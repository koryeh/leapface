package org.biu.neuroscience.leapface.experiment;

import java.io.Serializable;
import java.util.List;

import org.biu.neuroscience.leapface.conf.ConfCapsule;
import org.biu.neuroscience.leapface.experiment.face.Face;


/* Savable part of a running experiment.
 * contains a face and a list of tapes.
 * we need a list of tapes since every time we pause and resume the leap adapter
 * the timestamps may start from very different numbers.
 * once an experiment is ended we use glueTapes of ExpRunner to shift the timestamps
 * on all tapes so that they are continous */
public class ExpData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -251342449541728327L;

	private Face face;
	private List<List<FFrame>> tapes;

	
	public Face getFace() {
		return face;
	}
	public void setFace(Face face) {
		this.face = face;
	}
	public List<List<FFrame>> getTapes() {
		return tapes;
	}
	public void setTapes(List<List<FFrame>> tapes) {
		this.tapes = tapes;
	}
	public ConfCapsule getConf() {
		return face.getConf();
	}

	
	
	
}
