package org.biu.neuroscience.leapface.experiment.processing;

import java.io.Serializable;
import java.util.Map;

import org.apache.commons.lang3.Range;


/* A block of time in which there was no change.
 * each analyzed block comprises the range of timestamps
 * that bound it, and a map from part numbers to the number
 * of fingers on each */
public class AnalyzedBlock implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5457553718231162288L;
	
	/* range from block beginning to block end*/
	Range<Long> timeRange;
	Map<Integer,Integer> partToFingerAmountOnItMap;
	int index;
	
	
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public Range<Long> getTimeRange() {
		return timeRange;
	}
	public void setTimeRange(Range<Long> timeRange) {
		this.timeRange = timeRange;
	}
	public Map<Integer, Integer> getPartToFingerAmountOnItMap() {
		return partToFingerAmountOnItMap;
	}
	public void setPartToFingerAmountOnIt(
			Map<Integer, Integer> partToFingerAmountOnIt) {
		this.partToFingerAmountOnItMap = partToFingerAmountOnIt;
	}

	/* returns the length of the block (in microseconds) */
	public long getTimespan() {
		return timeRange.getMaximum()-timeRange.getMinimum();
	}
	
	
}
