package org.biu.neuroscience.leapface.experiment;

import java.io.Serializable;

/* Facade class for com.leapmotion.leap.Finger,
 * comprising less data and functionality.
 * We keep a Point object representing the finger tip,
 * an ID of the finger as it is given from the leap controller
 * and a hand id. Finger id stays the same as long as the controller
 * sees a finger continuously */
public class FFinger implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3050988287921844382L;
	private Point tip;
	private Integer id;
	private Integer hand;
	
	public FFinger(Integer id, Integer hand, Float x, Float y, Float z) {
		super();
		this.id = id;
		this.hand = hand;
		this.tip = new Point(x,y,z);
	}

	public Point getTip() {
		return tip;
	}

	public void setTip(Point tip) {
		this.tip = tip;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getHand() {
		return hand;
	}

	public void setHand(Integer hand) {
		this.hand = hand;
	}

	@Override
	public String toString() {
		return "FFinger [tip=" + tip.getX()+","+tip.getY()+","+tip.getZ() + ", id=" + id + ", hand=" + hand + "]";
	}
	
	
}
