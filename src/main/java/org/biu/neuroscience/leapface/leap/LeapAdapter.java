package org.biu.neuroscience.leapface.leap;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.Semaphore;

import org.apache.log4j.Logger;
import org.biu.neuroscience.leapface.conf.ConfCapsule;
import org.biu.neuroscience.leapface.experiment.FFinger;
import org.biu.neuroscience.leapface.experiment.FFrame;

import com.leapmotion.leap.Controller;
import com.leapmotion.leap.Finger;
import com.leapmotion.leap.Frame;
import com.leapmotion.leap.Listener;

/* This class functions as an adapter for the leap motion hardware.
 * It has an inner class inheriting from com.leapmotion.leap.Listener that
 * contains the custom functionality for leap motion events.
 * read documentation on each field */
public class LeapAdapter {
	
	public static final Logger LOGGER = Logger.getLogger(LeapAdapter.class);
	
	//a listener for the controller
	private SimpleListener lis;
	//a leap motion controller, instantiating it creates a connection with the leap motion service
	private Controller controller;
	/* A list of two tapes, making it possible to snatch the tape while the leap recording without losing
	 * data. this process involves acquiring a semaphore for concurrency safety.	 */
	private List<List<FFrame>> tapes = new ArrayList<List<FFrame>>();
	/* A semaphore for handling tapes. to add a new FFrame to the tape,
	 * or to change the tape the lock has to be acquired. */
	private Semaphore tapeChangingLock = new Semaphore(1);
	/* The tape currently in use, can be either one or zero. */
	private int tapeUsed=0;
	
	
	/* a count of frames used to know if we need to updated our displays, according
	 * to the displayUpdateCycle configuration */
	private int frameCount=0;	
	//how frequently the display is updated, this configuration is loaded from the conf file
		private int displayUpdateCycle;
	//Display is an inner class extending observable	
	private Display display = new Display();
	//current display option
	private int displayOption=0;
	//display options
	//if this option is selected the observers receive a raw FFrame
	public static final int RAW_FFRAME_DISPLAY=1;
	/* the advantage of formatted string display is that the string
	 * is created when together with the general analysis of it so 
	 * it spares the need to reiterate through the frame to monitor it	 */
	public static final int FORMATED_STRING_DISPLAY = 0;

	//A configuration capsule which has to be supplied to create an instance
	private ConfCapsule conf;
	
	
	public LeapAdapter(ConfCapsule conf) {
		this.conf=conf;
		displayUpdateCycle = this.conf.getDisplayUpdateCycle();
	
	}
	
	

	
	
	
	private class Display extends Observable {
		public void setOutput(Object output) {
			setChanged();	
			notifyObservers(output);
		}
	}
	
	
	
	/* custom listener class to be used by the adapter */
	class SimpleListener extends Listener {


	    public void onFrame(Controller controller) {

	        Frame frame = controller.frame();
	        
	        frameCount++;
	        
	        
	        FFrame fFrame = new FFrame();
	        fFrame.setTimestamp(frame.timestamp());
	        String output="";
	        boolean updateOutput=(frameCount%displayUpdateCycle==0); 
        	for (Finger finger : frame.fingers()) {
        		float x=finger.tipPosition().getX();
        		float y=finger.tipPosition().getY();
        		float z=finger.tipPosition().getZ();
        		FFinger fFinger = new FFinger(finger.id(),finger.hand().id(),x,y,z);
        		fFrame.addFinger(fFinger);
        		
        		if (updateOutput) {
        			if (displayOption==FORMATED_STRING_DISPLAY) {
            			output+="X: "+ x +" | Y: "+y+" | Z: "+z+"\n";
        			}
        		}
        		
        	}
        	
        	if (updateOutput) {
        		if (displayOption==FORMATED_STRING_DISPLAY) {
                	if (frame.fingers().isEmpty()) 	
            			display.setOutput("");	
                	else
                		display.setOutput(output);
        		}
    	        if (displayOption==RAW_FFRAME_DISPLAY)
    	        	display.setOutput(fFrame);
        	}

        	
        		
        		

        	
        		try {
					tapeChangingLock.acquire();
				} catch (InterruptedException e) {
					// ain't nobody gonna interrupt us here
					LOGGER.fatal("this was not supposed to happen", e);
					
				}
	        		tapes.get(tapeUsed).add(fFrame);
	        	tapeChangingLock.release();


	    }
	}

	
	/* This method verifies that the leap controller is available.
	 * it does a sleep loop until a timeout and checks each time if
	 * the service is available. */
	public static boolean isLeapConnected(int timeoutMilli) {
		Controller controller = new Controller();
		int waited=0;
		try {
			while (waited<timeoutMilli) {
			Thread.sleep(100);
			if (controller.isConnected() && controller.isServiceConnected())
				return true;
			
			waited+=100;
			}
			
			return false;
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
	}
	
	
	
	public void rec() {
		tapes.add(new ArrayList<FFrame>());
		tapes.add(new ArrayList<FFrame>());
		lis = new SimpleListener();
		controller = new Controller();
		controller.addListener(lis);
		
	}
	
	public void rec(Observer displayObserver, int displayOption) {
		this.displayOption=displayOption;
		display.addObserver(displayObserver);
		rec();
	}
	
	
	public List<FFrame> stop() {
		controller.removeListener(lis);

		try {
			tapeChangingLock.acquire();
		} catch (InterruptedException e) {
			// ain't nobody gonna interrupt us here
			LOGGER.fatal("this was not supposed to happen", e);
			
		}
		List<FFrame> rTape = tapes.get(tapeUsed);
		tapeChangingLock.release();
		tapes =  new ArrayList<List<FFrame>>();
		display.deleteObservers();
		return rTape;
	}
	
	/* acquires the lock, changes the tape, gets the old one, releases the lock
	 */
	public  List<FFrame> getTape() {
		
		//remember what tape was used
		int previousTape=tapeUsed;
		//change tape
		
		try {	
			tapeChangingLock.acquire();
		} catch (InterruptedException e) {
			// ain't nobody gonna interrupt us here
			LOGGER.fatal("this was not supposed to happen", e);
			
			
		}
		//change tapes
		tapeUsed=1-tapeUsed;
		//get the old tape
		List<FFrame> rTape = tapes.get(previousTape);
		//remove it from tapes
		tapes.remove(previousTape);
		//put a new one in
		tapes.add(previousTape,new ArrayList<FFrame>());
		tapeChangingLock.release();

		return rTape;
		
		
	
	}
	
	
	
	
	
	
	
	
	
	
	
}
