package org.biu.neuroscience.leapface.ui;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.text.DefaultCaret;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.SwingConstants;


/* This panel is the main panel for the experiment view
 * it is very simple compared to calibration panel.
 * it contains a few buttons, a timer label and a console */
public class ExperimentPanel extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1149465915026562139L;
	JButton btnRec;
	JButton btnPause;
	JButton btnStop;
	JScrollPane scrollPane;
	JTextArea console;
	JLabel lblTimer;
	
	
	
	public ExperimentPanel() {
		
		btnRec = new JButton("Rec");
		btnRec.setToolTipText("Record");
		btnRec.setIcon(new ImageIcon(ExperimentPanel.class.getResource("/icons/rec.png")));
		
		btnPause = new JButton("Pause");
		btnPause.setToolTipText("Pause");
		btnPause.setIcon(new ImageIcon(ExperimentPanel.class.getResource("/icons/pause.png")));
		
		btnStop = new JButton("Stop");
		btnStop.setToolTipText("Stop");
		btnStop.setIcon(new ImageIcon(ExperimentPanel.class.getResource("/icons/stop.png")));
		
		scrollPane = new JScrollPane();
		
		lblTimer = new JLabel("00:00");
		lblTimer.setHorizontalAlignment(SwingConstants.CENTER);
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnRec)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnPause)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnStop)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblTimer, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
						.addComponent(lblTimer, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(btnRec, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(btnPause, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(btnStop, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		console = new JTextArea();
		console.setFont(new Font("Consolas", Font.PLAIN, 13));
		console.setWrapStyleWord(true);
		console.setLineWrap(true);
		((DefaultCaret) console.getCaret()).setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		scrollPane.setViewportView(console);
		setLayout(groupLayout);
	}
}
