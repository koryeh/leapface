package org.biu.neuroscience.leapface.ui.calibration;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import org.apache.log4j.Logger;


/* the canvas for pen action.
 * when the user works with the pen it immediately adds pixels to the image that
 * is drawn as part of the paintComponent of this canvas. But since we need
 * to support the removal of lines that didn't snap closed (and also to keep each curve as a curve),
 * we also keep a list of lists of points (each list of points is a curve), and whenever a line doeasn't
 * snap we redraw the image from this list. In addition, since the part of the curve that was added when
 * it snapped isn't really input by the user, we keep the coordinated of these lines to redraw on this
 * canvas (when it is just a suggestion it is drawn on the upper layer, the snapCurveCanvas. */
public class CalibrationPenCanvasPanel extends JPanel implements CalibrationBrushableCanvasPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8637860928349757780L;
	
	
	public static final Logger LOGGER = Logger.getLogger(CalibrationPenCanvasPanel.class);
	
	
	/*package*/ BufferedImage image;
	/*package*/ Color background;
	
	/*during the use of the pen tool pixels will be drawn on this panel immediately
	 * but also added to lists for the purpose of undoing them	 */
	/*package*/ List<List<int[]>> curves = new ArrayList<List<int[]>>();
	/* snap lines are drawn on a layer above the penCanvas, but once they were snapped
	 * they are moved to this layer, so they are kept here	 */
	/*package*/ List<int[]> snappedLines = new ArrayList<int[]>();
	/*package*/ Color foreground;


	

	
	public CalibrationPenCanvasPanel(Color background, Color foreground) {
		this.foreground =foreground;
		
	}
	
	public void init() {
		snappedLines = new ArrayList<int[]>();
		curves = new ArrayList<List<int[]>>();
		image =null;
		repaint();
	}
	
	public BufferedImage getImage() {
		return image;
	}
	
	public void addSnappedLine(int[] line) {
		snappedLines.add(line);
	}
	
	public void recreateImageFromCurves() {
		BufferedImage newImage = new BufferedImage(image.getWidth(),image.getHeight(),BufferedImage.TYPE_INT_ARGB);
		WritableRaster raster = newImage.getRaster();
		
		
		
		for (List<int[]> curve : curves) {
			for  (int[] point : curve) {
				raster.setPixel(point[0], point[1], new int[]{foreground.getRed(),foreground.getBlue(),foreground.getGreen(),foreground.getAlpha()});

			}
		}

		image = newImage;
	
		repaint();
	}

    
    public void paintComponent(Graphics g) {
 
        super.paintComponent(g);     
       
       g.drawImage(image,0,0,background,this);
       
       Graphics2D g2d = (Graphics2D) g;
       g2d.setStroke(new BasicStroke(1));
       g2d.setColor(foreground);
       for (int [] line : snappedLines) {
    	   g2d.drawLine(line[0], line[1], line[2], line[3]);  	   
       }
    }  
}
