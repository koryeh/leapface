package org.biu.neuroscience.leapface.ui.calibration;

import java.awt.Color;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JTextArea;

/* a frame with instructions about the calibration process */
public class InstructionsFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7377338816490411810L;
	JTextArea txtrHitInitialize;
	public InstructionsFrame() {
		setAlwaysOnTop(true);
		
		txtrHitInitialize = new JTextArea();
		txtrHitInitialize.setWrapStyleWord(true);
		txtrHitInitialize.setText("\r\n\u2666 Hit initialize corners and follow instructions to initialize the corners of the rectangle containing the face\r\n\u2666 The i button in the toolbar on the left will open a window that will display coordinates captured from the leap controller.\r\n\u2666 Once the rectangle is initialized hit record and start touching the face. your finger tips will be rendered online. once you have enough of the face hit stop (you can hit pause if you think you might need more image later).\r\n\u2666 Use the pen to mark face parts. create closed, non-self-intersecting curves. when a curve is near its starting point it will suggest to snap closed.\r\n\u2666 After a curve is closed you'll be prompted to choose the part of the face that curve represents.\r\n\u2666 To cancel a curve use the undo button\r\n\u2666 To enable the \"done\" button hit the stop button.\r\n\u2666 When you are done marking face parts hit done. You will be asked if you'd like to save the face for later use.\r\n");
		txtrHitInitialize.setLineWrap(true);
		txtrHitInitialize.setFont(new Font("Courier New", Font.BOLD, 12));
		txtrHitInitialize.setEditable(false);
		txtrHitInitialize.setColumns(10);
		txtrHitInitialize.setBackground(new Color(240,240,240,240));
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(txtrHitInitialize, GroupLayout.DEFAULT_SIZE, 424, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(txtrHitInitialize, GroupLayout.PREFERRED_SIZE, 262, Short.MAX_VALUE)
		);
		getContentPane().setLayout(groupLayout);
	}
}
