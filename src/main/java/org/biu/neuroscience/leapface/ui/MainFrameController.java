package org.biu.neuroscience.leapface.ui;

import java.awt.CardLayout;
import java.awt.Desktop;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.biu.neuroscience.leapface.experiment.ExpData;
import org.biu.neuroscience.leapface.experiment.face.Face;
import org.biu.neuroscience.leapface.experiment.processing.ExpResult;
import org.biu.neuroscience.leapface.ui.calibration.CalibrationPanelController;


/* A controller for the main frame.
 * this controllers handles the menu and also the
 * transition between the cards in the cards layout of main frame (see MainFrame). */
public class MainFrameController {

	public static final Logger LOGGER = Logger
			.getLogger(MainFrameController.class);

	// views
	private MainFrame frame;
	private AboutFrame aboutFrame;

	// controllers this controller should know
	private CalibrationPanelController calibPanelController;
	private ExperimentPanelController expPanelController;
	private ResultPanelController resPanelController;
	
	
	private static final String manualPath="Manual.pdf";

	public MainFrameController(MainFrame frame) {
		super();
		this.frame = frame;
		frame.mntmExit.addActionListener(new ExitActionListener());
		frame.mntmNewExperiment
				.addActionListener(new NewExperimentActionListener());
		frame.mntmAbout.addActionListener(new AboutActionListener());
		frame.mntmManual.addActionListener(new ManualActionListener());
		frame.mntmOpen.addActionListener(new OpenActionListener());

		//disable menu stuff
		frame.mntmSave.setEnabled(false);
		frame.mntmExportFace.setEnabled(false);
		
		// about frame initialization
		aboutFrame = new AboutFrame();
		aboutFrame.setVisible(false);
		aboutFrame.setSize(470, 250);
		aboutFrame.setLocation((int) (GraphicsEnvironment
				.getLocalGraphicsEnvironment().getMaximumWindowBounds()
				.getWidth() / 2)
				- aboutFrame.getWidth(), (int) (GraphicsEnvironment
				.getLocalGraphicsEnvironment().getMaximumWindowBounds()
				.getHeight() / 2)
				- (aboutFrame.getHeight() / 2));
	}

	public void setCalibPanelController(
			CalibrationPanelController calibPanelController) {
		this.calibPanelController = calibPanelController;
	}

	public void setExpPanelController(
			ExperimentPanelController expPanelController) {
		this.expPanelController = expPanelController;
	}
	
	public void setResPanelController(
			ResultPanelController resPanelController) {
		this.resPanelController = resPanelController;
	}


	private class ExitActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			frame.dispatchEvent(new WindowEvent(frame,
					WindowEvent.WINDOW_CLOSING));
		}
	}

	/* handles actions from the open menu option.
	 * chooses flow according to file type.
	 * if a face file was loaded we start a new experiment with that face.
	 * if an existing experiment was loaded we continue this experiment.
	 * if a results file was loaded we go to the results view	 */
	private class OpenActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JFileChooser openChooser = new JFileChooser();
			openChooser
					.setFileFilter(new FileNameExtensionFilter(
							"Calibration (lfc), experiment (lfe) or result (lfr) files",
							"lfc", "lfe", "lfr"));
			if (openChooser.showDialog(frame, "open") == JFileChooser.APPROVE_OPTION) {
				String ext = FilenameUtils.getExtension(openChooser
						.getSelectedFile().toString());

				try (InputStream file = new FileInputStream(openChooser.getSelectedFile());
						InputStream buffer = new BufferedInputStream(file);
						ObjectInput input = new ObjectInputStream(buffer);) {
					Object obj = input.readObject();
					switch (ext) {
					case "lfc":
						Face face = (Face) obj;
						expPanelController.initialize(face);
						gotoExperiment();
						break;
					case "lfe":
						ExpData expData = (ExpData) obj;
						expPanelController.initialize(expData);
						gotoExperiment();
						break;
					case "lfr":
						ExpResult result = (ExpResult) obj;
						resPanelController.initialize(result);
						gotoResult();
						break;
					}

				} catch (Exception er) {
					JOptionPane.showMessageDialog(frame, "Couldn't load file");
					LOGGER.error("couldn't load file", er);
				}

			}

		}

	}

	private class NewExperimentActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			calibPanelController.initialize();
			gotoCalibrtion();
		}
	}

	public void gotoCalibrtion() {
		//remove save action listeners
	    for( ActionListener al : frame.mntmSave.getActionListeners() ) {
	    	frame.mntmSave.removeActionListener(al);
	    }
		
		
		
		frame.mntmSave.setEnabled(false);
		frame.mntmExportFace.setEnabled(false);
		CardLayout cards = (CardLayout) frame.getContentPane().getLayout();
		cards.show(frame.getContentPane(), "calibPanel");
		calibPanelController.initialize();
		frame.setTitle("Leapface - calibration");


	}

	public void gotoExperiment() {
		//remove save and export action listeners
	    for( ActionListener al : frame.mntmSave.getActionListeners() ) {
	    	frame.mntmSave.removeActionListener(al);
	    }
	    for( ActionListener al : frame.mntmExportFace.getActionListeners() ) {
	    	frame.mntmExportFace.removeActionListener(al);
	    }
		
		
		
		frame.mntmSave.setEnabled(true);
		frame.mntmExportFace.setEnabled(true);
		CardLayout cards = (CardLayout) frame.getContentPane().getLayout();
		cards.show(frame.getContentPane(), "expPanel");
		frame.setTitle("Leapface - experiment");
		frame.mntmSave.addActionListener(new ExperimentSaveActionListener());
		frame.mntmExportFace.addActionListener(new ExpExportFaceActionListener());
		
		frame.expPanel.console.setText("");
	}

	public void gotoResult() {
		//remove save and export action listeners
	    for( ActionListener al : frame.mntmSave.getActionListeners() ) {
	    	frame.mntmSave.removeActionListener(al);
	    }
	    for( ActionListener al : frame.mntmExportFace.getActionListeners() ) {
	    	frame.mntmExportFace.removeActionListener(al);
	    }
		
		
		frame.mntmSave.setEnabled(true);
		frame.mntmExportFace.setEnabled(true);
		CardLayout cards = (CardLayout) frame.getContentPane().getLayout();
		cards.show(frame.getContentPane(), "resPanel");
		frame.setTitle("Leapface - result");
		frame.mntmSave.addActionListener(new ResultSaveActionListener());
		frame.mntmExportFace.addActionListener(new ResExportFaceActionListener());
		
	}

	
	private class AboutActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			aboutFrame.setVisible(true);
		}
	}
	
	private class ManualActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				if (!(new File(manualPath)).exists()) {
					JOptionPane.showMessageDialog(frame, "Cant find " + manualPath +". Did you delete it or change its name?");
					return;		
				}
				Desktop.getDesktop().open(new File(manualPath));
			} catch (IOException e1) {
				LOGGER.error("Couldn't open manual file",e1);
			}
		}
	}
	
	
	private class ExperimentSaveActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			expPanelController.save();
		}
	}
	
	private class ResultSaveActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			resPanelController.save();
		}
	}
	
	
	private class ExpExportFaceActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			expPanelController.exportFace();
		}
	}

	private class ResExportFaceActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			resPanelController.exportFace();
		}
	}

	
	

}
