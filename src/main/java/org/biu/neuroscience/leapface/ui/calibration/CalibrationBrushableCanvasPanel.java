package org.biu.neuroscience.leapface.ui.calibration;

import java.awt.image.BufferedImage;

/* an interface that both CalibrationPenCanvasPanel andCalibrationFaceCanvasPanel
 * realize. the interface enables using the brush method of CalibrationUtils on both of them. */
public interface CalibrationBrushableCanvasPanel {
	public BufferedImage getImage();
	public void repaint(int x,int y,int width,int height);
	public int getWidth();
	public int getHeight();
}
