package org.biu.neuroscience.leapface.ui;


import java.awt.EventQueue;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;

import javax.swing.JFrame;

import org.apache.log4j.Logger;
import org.biu.neuroscience.leapface.conf.ConfCapsule;
import org.biu.neuroscience.leapface.leap.NativeLoader;
import org.biu.neuroscience.leapface.ui.calibration.CalibrationPanelController;
import org.ini4j.Wini;

/* This class is the entry point the the application.
 * it is responsible for lodaing the native libraries,
 * creating the views and the controllers,
 * and reading the configuration file. */
public class ApplicationLoader {

	public static final Logger LOGGER = Logger.getLogger(ApplicationLoader.class);
	private static final String PATH_TO_CONF="conf.ini";
	
	
	
	private static MainFrame frame;
	private static MainFrameController frameController;
	private static CalibrationPanelController calibrationPanelController;
	private static ExperimentPanelController expPanelController;
	private static ResultPanelController resPanelController;
	
	private static ConfCapsule conf;
	
    	/**
    	 * Launch the application.
    	 */
    	public static void main(String[] args) {

    		
  
    		LOGGER.info("Starting application...");
    		
    		LOGGER.info("Loading configuration...");
    		conf = loadConf();
    		
    		
    		EventQueue.invokeLater(new Runnable() {
    			public void run() {
    				try {
    					//set uncaught exception handler
    					Thread.currentThread().setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {

    					    public void uncaughtException(Thread t, Throwable e) {
    					    	Logger.getLogger(getClass().getEnclosingClass()).error(t + " throws exception: " + e,e);   
    					    }
    					 });
    		    		frame = new MainFrame();
    		    		frame.setBounds(0, 0, 450, 300);
    		    		frame.setExtendedState(JFrame.MAXIMIZED_BOTH); 
    		    		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    		    		frame.setVisible(true);
    		    		frameController = new MainFrameController(frame);
    		    		calibrationPanelController = new CalibrationPanelController(frame.calibPanel);
    		    		frameController.setCalibPanelController(calibrationPanelController);
    		    		expPanelController= new ExperimentPanelController(frame.expPanel);
    		    		frameController.setExpPanelController(expPanelController);
    		    		calibrationPanelController.setExpPanelController(expPanelController);
    		    		calibrationPanelController.setFrameController(frameController);
    		    		expPanelController.setFrameController(frameController);
    		    		resPanelController = new ResultPanelController(frame.resPanel);
    		    		expPanelController.setResultPanelController(resPanelController);
    		    		frameController.setResPanelController(resPanelController);

    				} catch (Exception e) {
    					LOGGER.error("Exception caught during application load", e);
    				}
    			}
    		});
    		
    		try {

			
			NativeLoader nl = new NativeLoader();
			nl.loadLibrary("msvcr120");
			nl.loadLibrary("msvcr120d");
			nl.loadLibrary("msvcp120");
			nl.loadLibrary("msvcp120d");
			nl.loadLibrary("Leap");
			nl.loadLibrary("Leapd");
			
			nl.saveLibrary("LeapJava");


    			
    			
			System.setProperty("java.library.path",System.getProperty("java.io.tmpdir"));
			/*since the wrapper leapmotion JNI classes make a static call to System.load 
			 * (probably) upon class load. manually loading the native libraries doesn't work, as the load
			 * is being overridden. the only way for the native libs to be loaded is via the
			 * java.library.path system property which is normally only read once, during the jvm startup.
			 * in order to change this variable programmatically and have it reread the ClassLoader member sys_paths
			 * has to be null. this can be done using reflection. for additional info on this technique:
			 * http://fahdshariff.blogspot.co.il/2011/08/changing-java-library-path-at-runtime.html
			 * The reason  we wanted the property to be set programmatically is for ease of deployment.
			 * we wanted the native libraries to be bundled in our package, with no additional configuration
			 * on insallation. */
		    final Field sysPathsField = ClassLoader.class.getDeclaredField("sys_paths");
		    sysPathsField.setAccessible(true);
		    sysPathsField.set(null, null);				

    		} catch (Exception e) {
    			LOGGER.error("Exception caught during application load", e);
    		}

    	}

    	
    	
    	private static ConfCapsule loadConf() {
    		try {
    			ConfCapsule returnConf = new ConfCapsule();
    			Wini wini = new Wini(new File(PATH_TO_CONF));
    			returnConf.setYSpan(Integer.parseInt(wini.get("face","y_span")));
    			returnConf.setFaceParts(wini.get("face","face_parts"));
    			returnConf.setMinimalVerticesDistance((Integer.parseInt(wini.get("face","minimal_vertices_distance"))));
    			
    			returnConf.setDialogOnRec(Boolean.parseBoolean(wini.get("workflow","dialog_on_rec=false")));
    			returnConf.setConsoleCharLim(Integer.parseInt(wini.get("workflow","console_char_lim")));
    			
    			returnConf.setDisplayUpdateCycle(Integer.parseInt(wini.get("leap","display_update_cycle")));
    			returnConf.setLeapConnectTimeout(Integer.parseInt(wini.get("leap","leap_connect_timeout")));
    			returnConf.setMinimalFingerPersistence(Integer.parseInt(wini.get("processing","minimal_finger_persistence")));
    			return returnConf;
    			
    		} catch (IOException e) {
    			LOGGER.error("Couldn't load configurations from conf file", e);
    			return null;
    		}
    		
    		
    		
    	}
    	
    	public static ConfCapsule getConf() {
    		return conf;
    		
    	}
    	
    	

}


















