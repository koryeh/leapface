package org.biu.neuroscience.leapface.ui.calibration;

/* A point in the face canvas's image's raster
 * not very special, it just calls the y dimension strength
 * which makes the code more readable*/
public class RasterPoint {
		private int x;
		private int z;
		private int strength;
		public RasterPoint(int x, int z, int strength) {
			super();
			this.x = x;
			this.strength = strength;
			this.z = z;
		}
		public int getX() {
			return x;
		}
		public void setX(int x) {
			this.x = x;
		}
		public int getStrength() {
			return strength;
		}
		@Override
		public String toString() {
			return "RasterPoint [x=" + x + ", strength=" + strength + ", z="
					+ z + "]";
		}
		public void setStrength(int strength) {
			this.strength = strength;
		}
		public int getZ() {
			return z;
		}
		public void setZ(int z) {
			this.z = z;
		}


}
