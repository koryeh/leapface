package org.biu.neuroscience.leapface.ui.calibration;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

/* canvas for the snap line suggestions.
 * if 'lineOn' then draw the line in 'cooridnates'
 * when repainting. if not don't. */
public class CalibrationSnapCurveCanvasPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8637860928349757780L;

	// initialized in CalibrationPanelController.InitializeCornersActionListener
	/* package */Integer[] coordinates = new Integer[4];
	/* package */boolean lineOn = false;
	

	
	private Color lineColor;
	
	
	

	
	public void init() {
		coordinates = new Integer[4];
		lineOn = false;
		repaint();
	}
	
	public CalibrationSnapCurveCanvasPanel(Color lineColor) {
		this.lineColor = lineColor;
	}

	public void paintComponent(Graphics g) {


		Graphics2D  g2d = (Graphics2D) g;
		
		if (lineOn) {
			g.setColor(lineColor);
			g2d.setStroke(new BasicStroke(5));
			g.drawLine(coordinates[0], coordinates[1], coordinates[2],
					coordinates[3]);
		}
		
		

	}
}
