package org.biu.neuroscience.leapface.ui.calibration;

import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Stack;
import java.util.concurrent.Semaphore;

import javax.swing.AbstractButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.math3.ml.distance.EuclideanDistance;
import org.apache.log4j.Logger;
import org.biu.neuroscience.leapface.conf.ConfCapsule;
import org.biu.neuroscience.leapface.experiment.FFinger;
import org.biu.neuroscience.leapface.experiment.FFrame;
import org.biu.neuroscience.leapface.experiment.Point;
import org.biu.neuroscience.leapface.experiment.face.Face;
import org.biu.neuroscience.leapface.experiment.face.FacePartDictionary;
import org.biu.neuroscience.leapface.leap.LeapAdapter;
import org.biu.neuroscience.leapface.ui.ApplicationLoader;
import org.biu.neuroscience.leapface.ui.ExperimentPanelController;
import org.biu.neuroscience.leapface.ui.MainFrame;
import org.biu.neuroscience.leapface.ui.MainFrameController;


/* Controller for the calibration panel, including the three canvases
 * This class implements the functionality of the calibration flow.
 * The flow starts with the user hitting the initialise corners button,
 * which prompts him with dialogs stating where he should put his fingers.
 * once it is done the user can hit record (and after that stop/pause), which
 * will make a LeapAdapter record his fingers. the user should then feel up the face
 * to get an image of it on faceCanvas. once this phase is paused or stopped the user
 * can use the pen (the functionality of which is in the PenActionListenre class) to
 * create curves that represent face parts. The drawn curve is drawn on PenCanvas (middle layer)
 * when a painted curve is almost closed a snap a suggestion will appear on
 *  snapCurveCanvas (top layer), if mouse released then the curve will stick and the user
 *  will be prompted with a dialog to choose what face part it was.
 *  the controller can be in one of 5 states:
 *  corners not initialised, corners initialised not started, started, paused, stopped
 *  the states determine what you can currently can and can't do. state change is done 
 *  using the changeState method
 */
public class CalibrationPanelController {
	
	public static final Logger LOGGER = Logger.getLogger(CalibrationPanelController.class);
	
	//the panel that's connected to this controller
	private CalibrationPanel panel;
	//a leap adapter instance for this controller
	private LeapAdapter adapter;
	/*a swing worker (which is basically a thread that can play nicely with the event dispatch thread,
	 * the thread responsible for dispatching events from the GUI, handling events, and updating it) that is
	 * responsible for rendering the face as it is captured online */
	private SwingWorker<Void, List<RasterPoint> > renderer;
	//frame containing this panel
	private MainFrame containingFrame;
	
	//controllers this controller should know
	private MainFrameController frameController;
	private ExperimentPanelController expPanelController;
	
	//prompt a dialog before recording to prepare hand?
	private boolean dialogOnRec;
	
	//face rectangle properties
	private Point tlCorner;
	private float recWidth;
	private float recHeight;
	private int ySpan;
	private float baseY;
	private float ceilingY;
	private float leftEdge;
	private float topEdge;
	
	//configuration, which is acquired from the ApplicationLoader.getConf() [static]
	ConfCapsule conf;
	
	//face parts map to create an array for the combo selection in the choose part dialog
	private Map<Integer,String> faceParts;
	//a face part dictionary created using conf (see FacePart)
	private FacePartDictionary facePartDict;
	//the face instance we are creating
	private Face face;
	/* A stack keeping record of the order in which we added parts, so that when we undo them we know
	 * what parts we should remove from face and add back the faceParts map	 */
	private Stack<Integer> partAddingOrder=new Stack<Integer>();
	/* the minimum distance between two adjecant vertices in a curve (when turning it into
	 * a boundary object. used to space out the vertices the user draws to make to polygonization
	 * quicker	 */
	private int minimalVerticesDistance;
	
	//an InfoFrame, a frame that displays finger tips location as is observed on the leap adapter's display
	private InfoFrame  infoFrame;
	//a frame with instructions for the calibration process
	private InstructionsFrame  instructionsFrame;	
	
	
	//the current state
	private int state=CORNERS_NOT_INITIALIZED;
	//states
	private static final int CORNERS_NOT_INITIALIZED=0;
	private static final int CORNERS_INITIALIZED_NOT_STARTED=1;
	private static final int STARTED=2;
	private static final int PAUSED=3;
	private static final int STOPPED=4;
	
	
	public CalibrationPanelController(final CalibrationPanel panel) {
		super();
		this.panel = panel;
		containingFrame = ((MainFrame) SwingUtilities.getWindowAncestor(panel));
		
		//listeners
		panel.canvasesContainer.addComponentListener(new PanelComponentAdapter());
		panel.btnInitializeCorners.addActionListener(new InitializeCornersActionListener());
		panel.btnRec.addActionListener(new RecActionListener());
//		panel.btnDebugInit.addActionListener(new DebugInit());
		panel.tglbtnInstructions.addActionListener(new InstructionsActionListener());
		panel.btnStop.addActionListener(new StopActionListener());
		panel.btnPause.addActionListener(new PauseActionListener());	
		panel.btnDone.addActionListener(new DoneActionListener());
		panel.btnUndo.addActionListener(new UndoActionListener());
		panel.tglbtnPen.addActionListener(new PenActionListener());
		panel.tglbtnInfoframe.addActionListener(new InfoActionListener());
		
		
		//aspectRatio
		panel.canvasAspectRatio=(float)panel.faceCanvas.getWidth()/(float)panel.faceCanvas.getHeight();
		

		
		//info frame initialization
		infoFrame = new InfoFrame();
		infoFrame.setVisible(false);
		infoFrame.setSize(200,200);
		infoFrame.setLocation((int) (GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().getWidth() - 
				infoFrame.getWidth()), 50);
		infoFrame.addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent arg0) {
				panel.tglbtnInfoframe.setSelected(false);
				
			}
		});
		
		//instructions frame initializaiton
		instructionsFrame = new InstructionsFrame();
		instructionsFrame.setVisible(false);
		instructionsFrame.setSize(500,300);
		instructionsFrame.setLocation(50,120);
		instructionsFrame.addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent arg0) {
				panel.tglbtnInstructions.setSelected(false);
				
			}
		});
		
		
		



		
	}
		
	public void setFrameController(MainFrameController frameController) {
		this.frameController = frameController;
	}

	public void setExpPanelController(ExperimentPanelController expPanelController) {
		this.expPanelController = expPanelController;
	}
	
	/* initialize the controller, the only way to get here is by selecting
	 * new experiment on the menu.	 */
	public void initialize() {
		//get configuration
		conf=ApplicationLoader.getConf();
		//create new adapter
		adapter = new LeapAdapter(conf);
		//initialize the canvases
		panel.faceCanvas.init();
		panel.penCanvas.init();
		panel.snapCurveCanvas.init();
		//set state of buttons
		panel.btnInitializeCorners.setEnabled(true);
		panel.tglbtnPen.setEnabled(false);
		panel.btnRec.setEnabled(false);
		panel.btnPause.setEnabled(false);
		panel.btnDone.setEnabled(false);
		panel.btnStop.setEnabled(false);		
		panel.btnUndo.setEnabled(false);
		panel.canvasesContainer.revalidate();
		//face parts initialisation
		facePartDict=new FacePartDictionary(conf);
		faceParts=facePartDict.getMap();

		
		//get ySpan, minimalVerticesDistance, and dialogOnRec from conf file 
		ySpan = conf.getYSpan();
		dialogOnRec = conf.isDialogOnRec();
		minimalVerticesDistance =  conf.getMinimalVerticesDistance();
		
		//set pen button to unselected
		if(panel.tglbtnPen.isSelected()) {
			panel.tglbtnPen.setSelected(false);
			unselectPenAction();
		}
		
		//go out of full screen if we are in full screen
		if (containingFrame.isUndecorated())
			containingFrame.unlockFullscreen();
		//create a new face object with our configuration capsule
		face = new Face(conf);
	}


	/* resize canvases when window size is changed. canvases container automatically
	 * streches vertically, so we need to take this new height and multiply it by the aspect
	 * ratio attribute of the Calibration panel to keep the same aspect ratio.
	 * the aspect ratio is set by the initialize corners flow	 */
	private class PanelComponentAdapter extends ComponentAdapter {
		@Override
		public void componentResized(ComponentEvent arg0) {
			
			if (panel.canvasAspectRatio!=0) {
				
				//the canvases container layered panel is vertically autoresizable (group layout)
				Integer height = panel.canvasesContainer.getHeight();
				Integer newWidth = (int) (height*panel.canvasAspectRatio);
		
				panel.faceCanvas.setBounds(panel.faceCanvas.getX(),panel.faceCanvas.getY(),newWidth,height);
				panel.penCanvas.setBounds(panel.penCanvas.getX(),panel.penCanvas.getY(),newWidth,height);
				panel.snapCurveCanvas.setBounds(panel.snapCurveCanvas.getX(),panel.penCanvas.getY(),newWidth,height);
				panel.faceCanvas.repaint();
				panel.canvasesContainer.revalidate();
			}
		}
		
	}
	
//	private class DebugInit implements ActionListener{
//		@Override
//		public void actionPerformed(ActionEvent e) {
//			containingFrame.lockToFullscreen();
//
//			
//			Point tl = new Point(-70f,215f,260f);
//			Point tr = new Point(110f,215f,260f);
//			Point bl = new Point(-70f,215f, 0f);
//			
//			Float verticalLength = Math.abs(tl.getZ()-bl.getZ());
//			Float horizontalLength = Math.abs(tl.getX()-tr.getX());
//			panel.canvasAspectRatio = horizontalLength/verticalLength;
//			
//			tlCorner = tl;
//			recWidth = horizontalLength;
//			recHeight = verticalLength;
//			baseY = tlCorner.getY();
//			//since the leap is placed on top (contrary to how it was designed),  going up means going down
//			ceilingY = tlCorner.getY() - ySpan;
//			leftEdge = tlCorner.getX();
//			topEdge = tlCorner.getZ();
//			
//			//set y range in face
//			face.setYRange(baseY, ceilingY);
//			
//			
//			//this should be invoked only after the lock to full screen has finished, and it seems it waits
//			//for this method to finish before it redraws the frame
//			EventQueue.invokeLater(new Runnable() {
//				@Override
//				public void run() {
//					Integer newWidth = (int) (panel.canvasesContainer.getHeight()*panel.canvasAspectRatio);
//					//resizing the canvas container and creating new images in canvases
//					panel.canvasesContainer.setBounds(panel.canvasesContainer.getX(),panel.canvasesContainer.getY(),newWidth,panel.canvasesContainer.getHeight());
//					panel.faceCanvas.image = new BufferedImage(newWidth,panel.faceCanvas.getHeight(),BufferedImage.TYPE_INT_ARGB);
//					panel.penCanvas.image = new BufferedImage(newWidth,panel.penCanvas.getHeight(),BufferedImage.TYPE_INT_ARGB);	
//				}			
//			});
//
//
//			changeState(CORNERS_INITIALIZED_NOT_STARTED);
//
//			
//		}
//	}
//	
	
	/* toggles the instrcutions frame */
	private class InstructionsActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			
			instructionsFrame.setVisible((!instructionsFrame.isVisible()));
			
		}
	}
	
	/* as we need to work inside a rectangle we need to know where that rectangle is.
	 * at this point we go to full screen so that the user won't be able to stretch the
	 * window and the such because then all hell will break loose (or in other words we don't 
	 * want to have to scale stuff). we show the info frame if it's not showing so that the user
	 * will see what he's doing. the user will be told to put his fingers on the top left,
	 * top right, and bottom left corners, sequentially, so we can derive their leap coordinates.
	 * after receiving those values we know are aspect ratio for the calibration and we can stretch
	 * the canvases accordingly (they always fill the height, so we just change them horizontally).
	 * we store the edges and dimensions of the rectanble in the fields marked "face rectangle properties".	 */
	private class InitializeCornersActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			
			//check that controller is connected
			if (!LeapAdapter.isLeapConnected(conf.getLeapConnectTimeout())) {
				JOptionPane.showMessageDialog(containingFrame, "Leap Motion controller not connected or service not running");
				return;		
			}
				
			containingFrame.lockToFullscreen();
			
			
			//show info frame
			if (!infoFrame.isVisible()) {
				infoFrame.setVisible(true);
				panel.tglbtnInfoframe.setSelected(true);
				
			}
			
			
			List<FFrame> tape;
			JOptionPane.showMessageDialog(panel, "Put one finger on the top left corner and hit ok");
			adapter.rec(new LeapDisplayObserver(),LeapAdapter.FORMATED_STRING_DISPLAY);
			JOptionPane.showMessageDialog(panel, "Hit ok and remove finger");
			tape = adapter.stop();
			Point tl = CalibrationUtils.getCornerFromTape(tape);
			if (tl.getX().isNaN()|| tl.getY().isNaN() || tl.getZ().isNaN()) {
				JOptionPane.showMessageDialog(panel, "Didn't receive point, make sure you see your finger in the info window");
				containingFrame.unlockFullscreen();
				initialize();
				return;
			}
				
			JOptionPane.showMessageDialog(panel, "Put one finger on the top right corner and hit ok, have it's Z value around "+ tl.getZ());
			adapter.rec(new LeapDisplayObserver(),LeapAdapter.FORMATED_STRING_DISPLAY);
			JOptionPane.showMessageDialog(panel, "Hit ok and remove finger");
			tape = adapter.stop();
			Point tr = CalibrationUtils.getCornerFromTape(tape);
			if (tr.getX().isNaN()|| tr.getY().isNaN() || tr.getZ().isNaN()) {
				JOptionPane.showMessageDialog(panel, "Didn't receive point, make sure you see your finger in the info window");
				containingFrame.unlockFullscreen();
				initialize();
				return;
			}
	
			JOptionPane.showMessageDialog(panel, "Put one finger on the bottom left corner and hit ok, have it's X value around " + tl.getX());
			adapter.rec(new LeapDisplayObserver(),LeapAdapter.FORMATED_STRING_DISPLAY);
			JOptionPane.showMessageDialog(panel, "Hit ok and remove finger");
			tape = adapter.stop();
			Point bl = CalibrationUtils.getCornerFromTape(tape);
			if (bl.getX().isNaN()|| bl.getY().isNaN() || bl.getZ().isNaN()) {
				JOptionPane.showMessageDialog(panel, "Didn't receive point, make sure you see your finger in the info window");
				containingFrame.unlockFullscreen();
				initialize();
				return;
			}
	
			
			
			
			
			
			Float verticalLength = Math.abs(tl.getZ()-bl.getZ());
			Float horizontalLength = Math.abs(tl.getX()-tr.getX());
			panel.canvasAspectRatio = horizontalLength/verticalLength;
			
			tlCorner = tl;
			recWidth = horizontalLength;
			recHeight = verticalLength;
			baseY = tlCorner.getY();
			//since the leap is placed on top (contrary to how it was designed),  going up means going down
			ceilingY = tlCorner.getY() - ySpan;
			leftEdge = tlCorner.getX();
			topEdge = tlCorner.getZ();
			

			//set y range in face
			face.setYRange(baseY, ceilingY);
			
			
			//this should be invoked only after the lock to full screen has finished, and it seems it waits
			//for this method to finish before it redraws the frame
			EventQueue.invokeLater(new Runnable() {
				@Override
				public void run() {
					Integer newWidth = (int) (panel.canvasesContainer.getHeight()*panel.canvasAspectRatio);
					//resizing the canvas container and creating new images in canvases
					panel.canvasesContainer.setBounds(panel.canvasesContainer.getX(),panel.canvasesContainer.getY(),newWidth,panel.canvasesContainer.getHeight());
					panel.faceCanvas.image = new BufferedImage(newWidth,panel.faceCanvas.getHeight(),BufferedImage.TYPE_INT_ARGB);
					panel.penCanvas.image = new BufferedImage(newWidth,panel.penCanvas.getHeight(),BufferedImage.TYPE_INT_ARGB);	
				}			
			});

						
			changeState(CORNERS_INITIALIZED_NOT_STARTED);

		}
	}
	
	
	
	/* when the record button is hit we first make sure the controller is available.
	 * if so we start the capturing and rendering process.
	 * we know the width and height of the rectangle and also where its edges are.
	 * we started a new swing worker that on every iteration takes the tape from the adapter,
	 * calculates the location of the points on the frames in the tape relative to the rectangle,
	 * and then translates that to a pixel coordinate with the same relation to the face canvas
	 * width and height. it draws the points in color (and alpha) that depend on the height of the point.
	 * points that are out of the rectangle or further than ySpan from the glass platform are ignored.	 */
	private class RecActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			
			if (!LeapAdapter.isLeapConnected(conf.getLeapConnectTimeout())) {
				JOptionPane.showMessageDialog(containingFrame, "Leap Motion controller not connected or service not running");
				return;		
			}
			
			
			if (dialogOnRec)	
				JOptionPane.showMessageDialog(panel, "Put your hand on the face and hit OK");

			changeState(STARTED);
			
			
			final int canvasHeight = panel.faceCanvas.getHeight();
			final int canvasWidth = panel.faceCanvas.getWidth();
			
			
			adapter.rec(new LeapDisplayObserver(),LeapAdapter.FORMATED_STRING_DISPLAY);	
			renderer = new SwingWorker<Void, List<RasterPoint> >() {
				
				/*each point is a leap point transformed to pixel location in the raster,
				 *  the y being a value from 1 to 255, according to it's size relative to yRange */
				List<RasterPoint> rasterPointsBuffer;
					@Override
					protected Void doInBackground() throws Exception {
						//will be stopped when the thread is cancelled from the stop button listener
						LOGGER.info("swing worker started");
						LOGGER.info("left edge: "+leftEdge);
						LOGGER.info("top edge:"+ topEdge);
						LOGGER.info("yFloor: "+ baseY);
						while (!isCancelled()) {
							rasterPointsBuffer = new ArrayList<RasterPoint>();
							List <FFrame> tape = adapter.getTape();
							for (FFrame frame : tape) {
								if (!frame.getFingers().isEmpty())
								for (FFinger finger : frame.getFingers()) {

									float x = finger.getTip().getX();
									float y = finger.getTip().getY();
									float z = finger.getTip().getZ();
									float relativeX=(x-leftEdge)/recWidth;
									float relativeZ=-(z-topEdge)/recHeight;
									float relativeY=(baseY-y)/ySpan;
															
									int rasterX = (int) (canvasWidth*relativeX);								
									int rasterZ = (int) (canvasHeight*relativeZ);
									int strength = (int) (255*relativeY);		
									
									
									
									if (rasterX>=0 && rasterX<canvasWidth && rasterZ>=0 && rasterZ<canvasHeight &&
											relativeY>=0 && relativeY<=1) {
										RasterPoint point = new RasterPoint(rasterX,rasterZ, strength);

										rasterPointsBuffer.add(point);

									}
									
								}	
							}
							publish(rasterPointsBuffer);	
						}
						return null;
				}	
				 
				@Override	
				protected void process(List<List<RasterPoint> > chunkList) {
					for (List<RasterPoint> chunk : chunkList) {
						for (RasterPoint point : chunk) {
							int a = point.getStrength() <100  ? 100 : 100+ point.getStrength()/255*155;
							CalibrationUtils.brush(panel.faceCanvas,point.getX(),point.getZ(),255,255-point.getStrength(),255,a,2);
						}
						
					}

							
				}
				
				@Override
				protected void done() {
					LOGGER.info("worker stopped");
				}
				
					 
			};
			renderer.execute();
			
		}
	}
	
	/* stop - we stop the renderer and the adapter, 
	 * and we enable the pen and done buttons
	 */
	private class StopActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			if (state!=PAUSED) {
				adapter.stop();
				renderer.cancel(true);
			}
			
			changeState(STOPPED);
			
		}
	}
	
	/* stop - we stop the renderer and the adapter, 
	 * and we enable the pen button	 */
	private class PauseActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			
			renderer.cancel(true);
			adapter.stop();
			panel.btnRec.setEnabled(true);

			changeState(PAUSED);
			
		}
	}
	
	/* this is an action listener for the pen toggle button in the toolbar.
	 * the methods for press, drag, and release actions are also inside this block,
	 * as they are called in the actionPerformed method of anonymous action listeners
	 * being registered to the penCanvas inside this action listener.
	 * What the pen does is let the user draw curves that meet the following requirements:
	 * they are closed
	 * they are not self intersecting
	 * they do not leave the rectangle.
	 * to make sure all curves are closed a curve is accepted only if it was snapped closed.
	 * snapping means the user was offered a closing line and he released the mouse when this suggestion
	 * was showing. the suggestion will appear only when the current mouse location is near where the curve started  */
	private class PenActionListener implements ActionListener {
		
		/* a semaphore use to lock the permission to draw on the canvas
		 * so that in certain cases new events will do nothing.
		 * this lock is only released on mouse release event so if 
		 * it is used it means lock stuff up until mouse is released */
		Semaphore paintingLock =  new Semaphore(1);
		
		//the x coordinate (pixels) of the mouse press
		int pressX;
		//the y coordinate of it (here y means what z usually means when we talk about leap dimension)
		int pressY;
		//the current x coordinate as we drag the mouse
		int currentX;
		//the current y
		int currentY;
		
		/* when the user starts his curve naturally the first points are in snap distance
		 * to the press point. we want to remove the snap suggestion from that case		 */
		boolean initialSnapSuggestion=true;
		
		EuclideanDistance distCalc = new EuclideanDistance();
		//the maximal distance from beginning to end of the curve for snap suggestion to appear
		int distanceToSnap=30;
		
		//snap line that stuck
		int[] snapLine =new int[4];
		
		/* when the user is dragging the mouse the pixels are drawn on the spot,
		 * but since we would also like to remove curves that weren't accepeted we remember
		 * each curve in the making like this, and only if it was accepted it is added to a list of
		 * curves in the pen canvas. every time a line is rejected to pen canvas redraws itself from that list
		 * of curves (and snap lines, the lines that were added as snap suggestions). */
		List<int[]> currentCurve;
		
		
		/* the action performed method which registers the methods below with the appropriate
		 * events on the canvas
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			if (((AbstractButton) e.getSource()).getModel().isSelected()) {
				//set a different cursor
				panel.snapCurveCanvas.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
				
				//add listeners, doesn't really matter to which canvas as they are all one on top of each other
		        panel.faceCanvas.addMouseListener(new MouseAdapter() {
		            @Override
					public void mousePressed(MouseEvent evt) {
		                canvasMousePressed(evt);
		            }
	
		            
		            @Override
					public void mouseReleased(MouseEvent evt) {
		                canvasMouseReleased(evt);
		            }
		        });
		        panel.faceCanvas.addMouseMotionListener(new MouseMotionAdapter() {
		            @Override
					public void mouseDragged(MouseEvent evt) {
		                canvasMouseDragged(evt);
		            }
		        });
			}
			else {
				unselectPenAction();
			}

	    }

		/* mouse pressed - save the press location, brush a dot there,
		 * and start a new current curve */
	    private void canvasMousePressed(MouseEvent evt) {
	    	pressX = evt.getX();
	    	pressY = evt.getY();
	    	CalibrationUtils.brush(panel.penCanvas,pressX,pressY,255,0,0,255,2);
	    	currentCurve=new ArrayList<int[]>();
	    
	    }
	    
	    /* mouse is dragged    */
	    private void canvasMouseDragged(MouseEvent evt) {
	    	//reset lineOn on snapCurveCanvas (the canvas that draws snap suggestions) to false,
	    	//so that if a new snap suggestion isn't called for the previous snap line will disappear
    		panel.snapCurveCanvas.lineOn=false;
    		
    		//see if the lock is available, don't wait if it isn't just leave
    		if (paintingLock.availablePermits()==0)
    			return;
    		//set current X and Y
             currentX = evt.getX();
             currentY = evt.getY();
             //paint the current dot
             CalibrationUtils.brush(panel.penCanvas,currentX,currentY,panel.penCanvas.foreground,1);
             //save the current dot as explained above
             currentCurve.add(new int[]{currentX,currentY});

             //if we are out of the rectangle
             if (currentX<0 || currentX> panel.penCanvas.getWidth() || currentY<0 || currentY >panel.penCanvas.getHeight()) {


            	//lock this method until mouse release event
				 paintingLock.tryAcquire();
				 return;
				

             }
            	 
             //suggest snap if needed
             if (distCalc.compute(new double[]{currentX,currentY},new double[]{pressX,pressY})<distanceToSnap) {
            	 if (!initialSnapSuggestion) {
            		 //set coordinates for snap line suggestions
	            	 panel.snapCurveCanvas.coordinates=new Integer[]{currentX,currentY,pressX,pressY};
	            	 //tell snap canvas to draw the stored line next time it repaints
	            	 panel.snapCurveCanvas.lineOn=true;
            	 }
             }
             else {
            	 /*  set initial snap suggestion to false. this actually changes something on the first time the
            	  *  initial justification to snap doesn't apply anymore (as a reminder, when a curve is started 
            	  *  a snap is allegedly justified because we are very close to the curve starting point */
            	 
            	 initialSnapSuggestion=false;
             }
             //repaint the snap curve canvas
             panel.snapCurveCanvas.repaint();
	    }


	    private void canvasMouseReleased(MouseEvent evt) {
	    	
	    	//release paintingLock if it's taken
	    	if (paintingLock.availablePermits()==0)
	    		paintingLock.release();
	    	
	    	//reinitialise initialSnapSuggestion
	    	initialSnapSuggestion=true;
	    	
    		/* there was a snap, and there are no intersections, so the curve is accepted.
    		 * see CalibrationUtils.isCurveSelfIntersecting to see how intersection detection is implemented */
    		if (panel.snapCurveCanvas.lineOn && !CalibrationUtils.isCurveSelfIntersecting(currentCurve)) {
    			

    			//add the snap suggestion as a permanent snap line, now in penCanvas and not snapCanvas
    			//we keep a list of all snapped line that stayed
    			snapLine[0] = evt.getX();
	            snapLine[1] = evt.getY();
	            snapLine[2] = pressX;
	            snapLine[3] = pressY;
	            panel.penCanvas.addSnappedLine(snapLine);
	            panel.penCanvas.curves.add(currentCurve);
	            snapLine=new int[4];
	            panel.snapCurveCanvas.lineOn=false;
	            panel.snapCurveCanvas.repaint();
	            panel.penCanvas.repaint();

	            
	            //select what part this curve represents
	    		String [] facePartsArray = new String[faceParts.size()];
	    		int i=0;
	    		for (String value : faceParts.values()) {
	    			facePartsArray[i]=value;
	    			i++;
	    		}
	    		
	            JComboBox<String> facePartsCombo = new JComboBox<String>(facePartsArray);
	            facePartsCombo.setSelectedIndex(0);
	            JOptionPane.showMessageDialog(null, facePartsCombo, "Choose face part",
	                     JOptionPane.QUESTION_MESSAGE);
	            
	            

	            
	            String selectedPart =(String) facePartsCombo.getSelectedItem();
	            Integer partNumber=facePartDict.getPartNumber(selectedPart);
	            faceParts.remove(partNumber);

	            //we are out of parts
	            if (faceParts.isEmpty()) {
	            	panel.tglbtnPen.setSelected(false);
	            	unselectPenAction();
	            	panel.tglbtnPen.setEnabled(false);
	            	
	            }
	            
	            //create a boundary from the curve using CalibrationUtils.curveToBoundary and add to face
	            face.addPart(partNumber, CalibrationUtils.curveToBoundary(currentCurve, minimalVerticesDistance,
	            		panel.faceCanvas.getWidth(), panel.faceCanvas.getHeight(), recWidth, recHeight, leftEdge, topEdge));
	            partAddingOrder.push(partNumber);
	            panel.btnUndo.setEnabled(true);
	            


    		}
    		//there wasn't a snap or there was an intersection, the curve should be erased
    		else {
    			panel.snapCurveCanvas.lineOn=false;
    			panel.faceCanvas.repaint();
    			panel.penCanvas.recreateImageFromCurves();
    			panel.faceCanvas.repaint();
    			panel.canvasesContainer.repaint();
    			panel.canvasesContainer.revalidate();
    		}
        
	    }	

	}
	
	/* undo action listeners. reenables the pen if it was disabled because we were
	 * out of parts, deltes the last part from the face (we know what part it was using
	 * the partAddingOrder stack), remove the snap line and curve that draw the boundary
	 * on the pen canvas and return the part to the availablt parts map faceParts.
	 * if there are no longer any parts in the face disable the undo button	 */
	private class UndoActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			
			if (panel.tglbtnPen.isSelected()) {
				panel.tglbtnPen.setSelected(false);
				unselectPenAction();
			}
			panel.penCanvas.curves.remove(panel.penCanvas.curves.size()-1);
			panel.penCanvas.snappedLines.remove(panel.penCanvas.snappedLines.size()-1);
			panel.penCanvas.recreateImageFromCurves();
			Integer lastAdded = partAddingOrder.pop();
			face.removePart(lastAdded);
			faceParts.put(lastAdded, facePartDict.getPartName(lastAdded));
			panel.tglbtnPen.setEnabled(true);
			if (panel.penCanvas.curves.isEmpty() || panel.penCanvas.snappedLines.isEmpty())
				panel.btnUndo.setEnabled(false);
			
		}
	}
	
	/* listener for the info frame toggle button
	 * 
	 */
	private class InfoActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			
			infoFrame.setVisible((!infoFrame.isVisible()));
			
		}
	}
	
	/* done action listener - suggest to save the face to a file,
	 * and either way (yes, no) continues to the experiment panel and initialises
	 * an experiment with the face */
	private class DoneActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			Integer option = JOptionPane.showConfirmDialog(containingFrame, "Would you like to save this face to a file?");
			//cancel
			switch (option) {
			case JOptionPane.CANCEL_OPTION: 
				return;
			case JOptionPane.YES_OPTION: 
				JFileChooser saveChooser = new JFileChooser();
				saveChooser
				.setFileFilter(new FileNameExtensionFilter(
						"calibration (lfc) file","lfc"));
				if (saveChooser.showSaveDialog(containingFrame)==JFileChooser.APPROVE_OPTION){
					File selectedFile = saveChooser.getSelectedFile();
					selectedFile = selectedFile.getName().endsWith(".lfc")? selectedFile : new File(selectedFile+".lfc");
					if (selectedFile.exists()) 
						//if selected cancel
						if(JOptionPane.showConfirmDialog(containingFrame, "File exists. overwrite?", "File exists", JOptionPane.OK_CANCEL_OPTION)==JOptionPane.CANCEL_OPTION)
							return;
						else
							selectedFile.delete();
					
				
					try {
						selectedFile.createNewFile();
						//try with resources statement which makes this is a JRE1.7 compliant project
						try (
						OutputStream fileStream = new FileOutputStream(selectedFile);
						OutputStream bufferedStream = new BufferedOutputStream(fileStream);
						ObjectOutput objectOutput = new ObjectOutputStream(bufferedStream);
							)
								{
									objectOutput.writeObject(face);
									expPanelController.initialize(face);
									instructionsFrame.setVisible(false);
									infoFrame.setVisible(false);
									frameController.gotoExperiment();
								}
						catch (Exception e2) {		
							LOGGER.error("couldn't write to file", e2);
						}
			
					} catch (IOException e1) {
						LOGGER.error("couldn't create file", e1);
					}					
				}
				else
					return;
				break;
			case JOptionPane.NO_OPTION: 
				expPanelController.initialize(face);
				instructionsFrame.setVisible(false);
				infoFrame.setVisible(false);
				frameController.gotoExperiment();
				break;
	
			}
			containingFrame.unlockFullscreen();
			
		}
	}
	
	/* an observer that feeds the info frame with the strings
	 * leap adapter puts on its display when the display option is formatted string */
	private class LeapDisplayObserver implements Observer {

		@Override
		public void update(Observable arg0, Object arg1) {
			String leapOutput = (String) arg1;
			infoFrame.setText(leapOutput);			
		}
	
	
	}
	
	
	/*should be called whenever pen is set to unselected programmatically,
	 * to change the cursor and the such (since the action listener method
	 * is not called) */
	private void unselectPenAction() {
		panel.faceCanvas.removeMouseListener(panel.faceCanvas.getMouseListeners()[0]);;
		panel.faceCanvas.removeMouseMotionListener(panel.faceCanvas.getMouseMotionListeners()[0]);;
		panel.snapCurveCanvas.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
	}


	/* changes the state of the controller, which sets what buttons
	 * are avialble or unavailable	 */
	private void changeState(int newState) {
		switch (newState) {
		case CORNERS_INITIALIZED_NOT_STARTED:
			panel.btnRec.setEnabled(true);
			break;
		case STARTED:
			panel.btnInitializeCorners.setEnabled(false);
			panel.btnRec.setEnabled(false);
			panel.btnPause.setEnabled(true);
			panel.btnStop.setEnabled(true);	
			panel.btnDone.setEnabled(false);
			if (panel.tglbtnPen.isSelected()) {
				panel.tglbtnPen.setSelected(false);		
				unselectPenAction();
			}
			panel.tglbtnPen.setEnabled(false);
			break;
		case PAUSED:
			panel.btnPause.setEnabled(false);
			panel.btnStop.setEnabled(true);
			panel.tglbtnPen.setEnabled(true);
			break;
		case STOPPED:
			panel.btnRec.setEnabled(false);
			panel.btnPause.setEnabled(false);
			panel.btnStop.setEnabled(false);
			panel.tglbtnPen.setEnabled(true);
			panel.btnDone.setEnabled(true);
			break;

		}
		state=newState;
	}
	
}
