package org.biu.neuroscience.leapface.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.log4j.Logger;
import org.biu.neuroscience.leapface.conf.ConfCapsule;
import org.biu.neuroscience.leapface.experiment.ExpData;
import org.biu.neuroscience.leapface.experiment.ExpRunner;
import org.biu.neuroscience.leapface.experiment.FFinger;
import org.biu.neuroscience.leapface.experiment.FFrame;
import org.biu.neuroscience.leapface.experiment.Point;
import org.biu.neuroscience.leapface.experiment.face.Face;
import org.biu.neuroscience.leapface.experiment.face.FacePartDictionary;
import org.biu.neuroscience.leapface.experiment.processing.AnalyzedBlock;
import org.biu.neuroscience.leapface.experiment.processing.DataProcessor;
import org.biu.neuroscience.leapface.experiment.processing.ExpResult;
import org.biu.neuroscience.leapface.leap.LeapAdapter;

/* A controller for the experiment panel
 * Contains inner classes for action listeners for the
 * buttons on the panel, a swing timer and an action listener for it, an observer extending display class,
 * references to the main frame, the panel and other controllers it should know (see fields),
 * and an instance of the ExpRunner class from the model.
 * the controller can be in one of four states, not started,started, paused, and stopped.
 * the states determine what buttons and menu options are available or unavailable. */
public class ExperimentPanelController {
	
	public static final Logger LOGGER = Logger.getLogger(ExperimentPanelController.class);
	
	
	
	//the panel we control (a constructor argument)
	private ExperimentPanel panel;
	//the expRunner instance, create upon initialisation
	private ExpRunner runner;
	//a LeapDisplayObserver instance to observe a leap adapter display
	private LeapDisplayObserver display;

	private MainFrame containingFrame;
	private MainFrameController frameController;
	//needed to initialize a resultPanelController what stop is hit
	private ResultPanelController resultController;
	
	//the minutes on the last time it started running
	private int timerNotRunningMin;
	//the seconds on the...
	private int timerNotRunningSec;
	//system time when we hit record, to calculate the new time on timer each time
	private long systemTimeOnRecHit;
	//a swing timer to dispatch events for the updating of the timer labels
	Timer timer;
	
	//number of characters in the panel's console, to be read from the configuration capsule
	private int consoleCharLim;
	//configuration
	private ConfCapsule conf;
	//a face part dictionary that will be created using conf
	private FacePartDictionary facePartDict;
	
	//the state we are in
	private int state=0;
	//states
	private static final int NOT_STARTED=0;
	private static final int STARTED=1;
	private static final int PAUSED=2;
	private static final int STOPPED=3;
	
	
	
	/* constructor, connects listeners to components
	 * 
	 */
	public ExperimentPanelController(ExperimentPanel panel) {
		super();
		this.panel = panel;
		containingFrame = ((MainFrame) SwingUtilities.getWindowAncestor(panel));
		panel.btnRec.addActionListener(new RecActionListener());
		panel.btnStop.addActionListener(new StopActionListener());
		panel.btnPause.addActionListener(new PauseActionListener());
		timer = new Timer(1000,new TimerActionListener());

		
		
	}
	
	/* initialise method for a new experiment.
	 * we can enter through here either if we just
	 * finished calibration or if we loaded an lfc (face)
	 * file	 */
	public void initialize(Face face) {
		state=NOT_STARTED;
		runner = new ExpRunner(face);
		display = new LeapDisplayObserver();
		panel.btnRec.setEnabled(true);
		panel.btnStop.setEnabled(false);
		panel.btnPause.setEnabled(false);
		conf=runner.getConf();
		facePartDict=new FacePartDictionary(conf);
		//get consoleCharLim from conf file 
		consoleCharLim = conf.getConsoleCharLim();
		
		//initialise timer
		timerNotRunningMin=0;
		timerNotRunningSec=0;
		updateTime(timerNotRunningMin,timerNotRunningSec);

	}
	/* initialise method for contiuing an experiment
	 * loaded from an lfe file. */
	public void initialize(ExpData expData) {
		state=PAUSED;
		runner = new ExpRunner(expData);
		display = new LeapDisplayObserver();
		panel.btnRec.setEnabled(true);
		panel.btnStop.setEnabled(true);
		panel.btnPause.setEnabled(false);
		conf=runner.getConf();	
		facePartDict=new FacePartDictionary(conf);
		//get consoleCharLim from conf file 
		consoleCharLim = conf.getConsoleCharLim();
		
		//initialise timer
		List<FFrame> lastTape =  expData.getTapes().get(expData.getTapes().size()-1);
			
		long lastTimestamp =  lastTape.get(lastTape.size()-1).getTimestamp();
		long lastTimestampInSec = (lastTimestamp)/1000000;
		

		
		timerNotRunningMin=(int) lastTimestampInSec/60;
		timerNotRunningSec=(int) lastTimestampInSec%60;
		updateTime(timerNotRunningMin,timerNotRunningSec);
	}
	
	

	public void setFrameController(MainFrameController frameController) {
		this.frameController=frameController;
		
	}
	public void setResultPanelController(ResultPanelController resultController) {
		this.resultController=resultController;
	}
	
	
	/* record - checks if leap is available and then starts runner,
	 * which in turn starts recording ot it's instance of a leap adapter.
	 * the timer is also started	 */
	private class RecActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			
			if (!LeapAdapter.isLeapConnected(conf.getLeapConnectTimeout())) {
				JOptionPane.showMessageDialog(containingFrame, "Leap Motion controller not connected or service not running");
				return;		
			}
					
			panel.btnPause.setEnabled(true);
			panel.btnStop.setEnabled(true);
			panel.btnRec.setEnabled(false);

			runner.start(display);

			
			//start timer
			systemTimeOnRecHit=System.currentTimeMillis();
			timer.start();
			
			state=STARTED;
			
			
			
		}
	}
	
	/* pause - stops runner and stops the timer	 */
	private class PauseActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			timer.stop();
			panel.btnPause.setEnabled(false);
			panel.btnStop.setEnabled(true);
			panel.btnRec.setEnabled(true);
			runner.stop();
			
			long currentSystemTime = System.currentTimeMillis();
			long differenceInSecs= (currentSystemTime-systemTimeOnRecHit)/1000;
			timerNotRunningMin+=(int)differenceInSecs/60;
			timerNotRunningSec+=(int)differenceInSecs%60;
			
			state=PAUSED;
			
		}
	}
	
	/* stops the runner, stops the timer,
	 * glues the tapes on runner (see ExpRunner),
	 * refines the tapes (see DataProcessor.refineData...)
	 * analyses the refined tape (see DataProcessor.analyzeData)
	 * creates a new ExpResult object.
	 * initialises ResultPanelController.
	 * makes the frame go to the resultPanelController card	 */
	private class StopActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			
			if (state!=PAUSED) {
				runner.stop();	
				timer.stop();
			}
				
			panel.btnPause.setEnabled(false);
			panel.btnStop.setEnabled(false);
			panel.btnRec.setEnabled(false);
			state=STOPPED;
			
			DataProcessor processor = new DataProcessor(runner.getConf());
			
			List<FFrame> refined = processor.refineData(runner.getExpData().getTapes());
			List<AnalyzedBlock> analyzedData = processor.analyzeData(refined, runner.getFace());
			ExpResult result = new ExpResult();
			result.setAnalyzedData(analyzedData);
			result.setRawData(runner.getExpData().getTapes());
			result.setRefinedData(refined);
			result.setFace(runner.getFace());
			resultController.initialize(result);
			frameController.gotoResult();
		}
	}
	
	
	/* A leap display observer that expects raw FFrames,
	 * and hence when using it the display option on the adapter has
	 * to be set accordingly. It formats the data on the frame and updates the 
	 * console component on the panel (a JTextArea). truncates the text so that is
	 * shorter than the console char limit	 */
	private class LeapDisplayObserver implements Observer {
		
		@Override
		public void update(Observable observable, Object newFrame) {
			FFrame frame = (FFrame) newFrame;
			String text = "**************************************" + frame.getTimestamp() + 
					"**************************************\n";
			for (FFinger finger : frame.getFingers()) {
				text+="\t+++ ";
				Point tip = finger.getTip();
				text = text + tip.getX()+","+tip.getY()+","+tip.getZ()+":";
				Face face = runner.getFace();
				if (!face.isEmpty())
				for (Integer part : face.whereIs(finger.getTip()))
						text = text + " " + facePartDict.getPartName(part);
			}
			if (!frame.isEmpty())
				text+="\n";
			String fullText = (panel.console.getText() + text);
			String truncText = fullText.substring(fullText.length()-consoleCharLim > 0? fullText.length()-consoleCharLim : 0);

			panel.console.setText(truncText);			
		}
	
	}



	/* saves the expData of the runner in an lfe file.
	 * this method can be called by the save as menu item action
	 * listener on MainFrame */
	public void save() {
		JFileChooser saveChooser = new JFileChooser();
		saveChooser
		.setFileFilter(new FileNameExtensionFilter(
				"experiment (lfe) file","lfe"));
		if (saveChooser.showSaveDialog(containingFrame)==JFileChooser.APPROVE_OPTION){
			File selectedFile = saveChooser.getSelectedFile();

			selectedFile = selectedFile.getName().endsWith(".lfe")? selectedFile : new File(selectedFile+".lfe");
			if (selectedFile.exists()) 
				//if selected cancel
				if(JOptionPane.showConfirmDialog(containingFrame, "File exists. overwrite?", "File exists", JOptionPane.OK_CANCEL_OPTION)==JOptionPane.CANCEL_OPTION)
					return;
				else
					selectedFile.delete();
			try {
				selectedFile.createNewFile();
				//try with resources statement which makes this is a JRE1.7 compliant project
				try (
				OutputStream fileStream = new FileOutputStream(selectedFile);
				OutputStream bufferedStream = new BufferedOutputStream(fileStream);
				ObjectOutput objectOutput = new ObjectOutputStream(bufferedStream);
					)
						{
						if (state==STARTED)
					 		runner.stop();
							objectOutput.writeObject(runner.getExpData());
						if (state==STARTED)
							runner.start(display);

						}
				catch (Exception e2) {		
					LOGGER.error("couldn't write to file", e2);
				}
	
			} catch (IOException e1) {
				LOGGER.error("couldn't create file", e1);
			}					
		}
	}
	
	
	/* saves the Face oobject of the ExpData object of the runner in an lfc file.
	 * this method can be called by the export face menu item action
	 * listener on MainFrame */
	public void exportFace() {
		JFileChooser saveChooser = new JFileChooser();
		saveChooser
		.setFileFilter(new FileNameExtensionFilter(
				"calibration (lfc) file","lfc"));
		if (saveChooser.showSaveDialog(containingFrame)==JFileChooser.APPROVE_OPTION){
			File selectedFile = saveChooser.getSelectedFile();
			selectedFile = selectedFile.getName().endsWith(".lfc")? selectedFile : new File(selectedFile+".lfc");
			if (selectedFile.exists()) 
				//if selected cancel
				if(JOptionPane.showConfirmDialog(containingFrame, "File exists. overwrite?", "File exists", JOptionPane.OK_CANCEL_OPTION)==JOptionPane.CANCEL_OPTION)
					return;
				else
					selectedFile.delete();
			try {
				selectedFile.createNewFile();
				//try with resources statement which makes this is a JRE1.7 compliant project
				try (
				OutputStream fileStream = new FileOutputStream(selectedFile);
				OutputStream bufferedStream = new BufferedOutputStream(fileStream);
				ObjectOutput objectOutput = new ObjectOutputStream(bufferedStream);
					)
						{
						if (state==STARTED)
					 		runner.stop();
							objectOutput.writeObject(runner.getExpData().getFace());
						if (state==STARTED)
							runner.start(display);

						}
				catch (Exception e2) {		
					LOGGER.error("couldn't write to file", e2);
				}
	
			} catch (IOException e1) {
				LOGGER.error("couldn't create file", e1);
			}					
		}
	}
	
	//receives minutes and seconds and updates it on the view
	public void updateTime(int min, int sec){
		panel.lblTimer.setText((min<10? "0"+min:min) +":" +( sec<10? "0"+sec:sec));	
	}
	
	/* an action listener for our swing timer.
	 * takes current system time, compares it to the system time
	 * when the timer was started and adds the elapsed time 
	 * to the time when the timer was started	 */
	private class TimerActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			
			long currentSystemTime = System.currentTimeMillis();
			long differenceInSecs= (currentSystemTime-systemTimeOnRecHit)/1000;
			updateTime(timerNotRunningMin+((int)differenceInSecs)/60,(timerNotRunningSec+(int)differenceInSecs)%60);
			
			
			
			
		}
	}
	
	

}
