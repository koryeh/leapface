package org.biu.neuroscience.leapface.ui;

import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.Range;
import org.apache.log4j.Logger;
import org.biu.neuroscience.leapface.conf.ConfCapsule;
import org.biu.neuroscience.leapface.experiment.face.FacePartDictionary;
import org.biu.neuroscience.leapface.experiment.processing.AnalyzedBlock;
import org.biu.neuroscience.leapface.experiment.processing.DataProcessor;
import org.biu.neuroscience.leapface.experiment.processing.ExpResult;

/* A controller for the results panel.
 * this controller holds a reference to the panel, and an ExpResult instance.
 * From that instance it can derive the proper configuration (stored inside the face object)
 * and create a face part dictionary and a data processor.
 * it has three actions listeners for the three types of queries described in ResultPanel */
public class ResultPanelController {


	public static final Logger LOGGER = Logger.getLogger(ResultPanelController.class);
	
	
	ResultPanel panel;
	ExpResult result;
	MainFrame containingFrame;
	ConfCapsule conf;
	FacePartDictionary facePartDict;
	DataProcessor processor;
	
	/* this constructor is called once, during application load.
	 * this is where action listeners are attached to components */
	public ResultPanelController(ResultPanel panel) {
		super();
		this.panel = panel;
		containingFrame = ((MainFrame) SwingUtilities.getWindowAncestor(panel));
		
		panel.btnTotalSubmit.addActionListener(new TotalActionListener());
		panel.btnPartRangesSubmit.addActionListener(new PartRangesActionListener());
		panel.btnRangePartsSubmit.addActionListener(new RangePartsActionListener());
	}
	
	/* initalize this controller with a result.
	 * this method is called when an experiment is finished 
	 * or when an lfr file is loaded */
	public void initialize(ExpResult result) {
		this.result=result;
		int experimentLengthSeconds=result.expLenghInSeconds();
		int minutes = experimentLengthSeconds/60;
		int seconds = experimentLengthSeconds%60;
		panel.lblExpLengthValue.setText((minutes<10? "0"+minutes:minutes)+":"+(seconds<10? "0"+seconds :seconds));
		conf=result.getConf();
		facePartDict=new FacePartDictionary(conf);
		processor=new DataProcessor(conf);
		
		for (String part : facePartDict.getParts()) {
			panel.comboPartRanges.addItem(part);
			panel.comboTotal.addItem(part);
		}
		
		
		for (int m=0;m<=minutes;m++) {
			panel.comboMin1.addItem(m);
			panel.comboMin2.addItem(m);
		}
		for (int sec=0;sec<60;sec++) {
			panel.comboSec1.addItem(sec);
			panel.comboSec2.addItem(sec);
		}
			
	}

	/* Action listener for the first type of query,
	 * sum the time that some part of the face was touched.
	 * it gets the answer in microseconds by calling DataProcessor.totalTimeSpentInPart(...)
	 * and formats it. then it creates a query result frame to display it */
	private class TotalActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			String selectedPartName = (String) panel.comboTotal.getSelectedItem();
			int partNum=facePartDict.getPartNumber(selectedPartName);
			String resultString = String.format("%.2f\n",((float)processor.totalTimeSpentInPart(result, partNum)/1000000));
			
			QueryResultFrame resultFrame = new QueryResultFrame(panel.lblTotalSpentIn.getText()+" "+selectedPartName, resultString);
			resultFrame.setVisible(true);
			resultFrame.setSize(250,150);
			resultFrame.setLocation((int) (GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().getWidth() - 
					resultFrame.getWidth()), 50);
		}
	}
	

	/* Gives all the time ranges in which a face part was touched.
	 * it glues contiguous blocks in which the same amount of fingers touched
	 * that given face part (ignoring what happened in different face parts).
	 * it gets a list of all the blocks in which a part was touched by calling
	 * data processor's partRanges(...)	 */
	private class PartRangesActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			String selectedPartName = (String) panel.comboPartRanges.getSelectedItem();
			int partNum=facePartDict.getPartNumber(selectedPartName);
			
			List<AnalyzedBlock> queryResult = DataProcessor.partRanges(result, partNum); 
			
			StringBuilder builder = new StringBuilder();
			
			int currentNumberOfFingersOnPart=-1;
			int newNumberOfFingers=-1;
			
			long blockStart=0;
			long blockEnd=0;
			int lastContiguousBlockIndex=-1;
			boolean first=true;
			String numOfFingers = "";
			AnalyzedBlock block = null;
			
			//most likely the ugliest loop i've ever written
			for (int i=0; i<queryResult.size()+1;i++) {
				
				/*the special case where i=queryResult.size() is used to handle the last block,
				 * since on each iteration we push the block before it if the current one is different etc. */	
				if (i!=queryResult.size()) {
					block = queryResult.get(i);
					newNumberOfFingers=ObjectUtils.defaultIfNull(block.getPartToFingerAmountOnItMap().get(partNum),0);
				}

				/* if it's the last block or if this block is different (in the amount of fingers on the part we check) from the previous blocks
				 * or if it is not contiguous with it */
				if (block.getIndex()!=lastContiguousBlockIndex+1 /*not contiguous*/||
						i==queryResult.size() /*last block*/|| newNumberOfFingers!=currentNumberOfFingersOnPart /*block is different*/) {
					//we deliver the last congiuous non different block chain
					int blockStartInCents = (int) (blockStart/10000);
					int blockEndInCents = (int) (blockEnd/10000);
					
					int blockStartMin = blockStartInCents/6000;
					int blockStartSec =  (blockStartInCents%6000)/100;
					int blockStartCents = (blockStartInCents%100);
					int blockEndStartMin = blockEndInCents/6000;
					int blockEndSec =  (blockEndInCents%6000)/100;
					int blockEndCents = blockEndInCents%100;
	
					
					
					String formattedRange=(blockStartMin<10? "0"+blockStartMin:blockStartMin)+":"+
											(blockStartSec<10? "0"+blockStartSec :blockStartSec)+":"+
											(blockStartCents<10? "0"+blockStartCents :blockStartCents)+
											"-"+	
											(blockEndStartMin<10? "0"+blockEndStartMin:blockEndStartMin)+":"+
											(blockEndSec<10? "0"+blockEndSec :blockEndSec)+":"+
											(blockEndCents<10? "0"+blockEndCents :blockEndCents);
					
					if (!first)
						builder.append(formattedRange).append(", ").append(numOfFingers).append("\n");
					else
						first=false;
					
					//if it isn't the "one after last block" we start the next block chain
					if (i!=queryResult.size()) {
						blockStart=block.getTimeRange().getMinimum();
						blockEnd=block.getTimeRange().getMaximum();
						numOfFingers= "Number of fingers on " + selectedPartName +": "+block.getPartToFingerAmountOnItMap().get(partNum)+".";
						currentNumberOfFingersOnPart=newNumberOfFingers;	
						lastContiguousBlockIndex=block.getIndex();
					}
					

				}
				else {
					blockEnd=block.getTimeRange().getMaximum();
					lastContiguousBlockIndex=block.getIndex();
				}
							
			} //it's hideous		
			
			
			
			String resultString=builder.toString();
			QueryResultFrame resultFrame = new QueryResultFrame(panel.lblPartRanges.getText()+" "+selectedPartName, resultString);
			resultFrame.setVisible(true);
			resultFrame.setSize(500,500);
			resultFrame.setLocation((int) (GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().getWidth() - 
					resultFrame.getWidth()), 50);
		}
	}
	
	
	
	
	
	
	/* gets all the blocks that are in a time range.
	 * it gets the blocks by calling DataProcessor.rangeParts(...)	 */
	private class RangePartsActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			int end1Min = (Integer) panel.comboMin1.getSelectedItem();
			int end1Sec = (Integer) panel.comboSec1.getSelectedItem();
			int end2Min = (Integer) panel.comboMin2.getSelectedItem();
			int end2Sec = (Integer) panel.comboSec2.getSelectedItem();
			
			Range<Long> range =Range.between((long) end1Min*60000000+end1Sec*1000000,(long) end2Min*60000000+end2Sec*1000000);
			List<AnalyzedBlock> queryResult = processor.rangeParts(result, range);
			
			
			
			long blockStart;
			long blockEnd;
			StringBuilder builder = new StringBuilder();
			
			for (AnalyzedBlock block : queryResult) {
	
				blockStart=block.getTimeRange().getMinimum();
				blockEnd=block.getTimeRange().getMaximum();
				
				int blockStartInCents = (int) (blockStart/10000);
				int blockEndInCents = (int) (blockEnd/10000);
				
				int blockStartMin = blockStartInCents/6000;
				int blockStartSec =  (blockStartInCents%6000)/100;
				int blockStartCents = (blockStartInCents%100);
				int blockEndStartMin = blockEndInCents/6000;
				int blockEndSec =  (blockEndInCents%6000)/100;
				int blockEndCents = blockEndInCents%100;

				
				
				String formattedRange=(blockStartMin<10? "0"+blockStartMin:blockStartMin)+":"+
										(blockStartSec<10? "0"+blockStartSec :blockStartSec)+":"+
										(blockStartCents<10? "0"+blockStartCents :blockStartCents)+
										"-"+	
										(blockEndStartMin<10? "0"+blockEndStartMin:blockEndStartMin)+":"+
										(blockEndSec<10? "0"+blockEndSec :blockEndSec)+":"+
										(blockEndCents<10? "0"+blockEndCents :blockEndCents);
				
				builder.append(formattedRange).append(", ");
				for (Map.Entry<Integer, Integer> entry : block.getPartToFingerAmountOnItMap().entrySet()) {
					
					builder.append(facePartDict.getPartName(entry.getKey()) +": "+entry.getValue()+" fingers. ");
					 
				}
				
	
				builder.append("\n");
				
				
			
			}			
			String resultString=builder.toString();
			QueryResultFrame resultFrame = new QueryResultFrame(panel.lblRangeParts.getText()+" "+end1Min+":"+end1Sec+" to "+end2Min+":"+end2Sec, resultString);
			resultFrame.setVisible(true);
			resultFrame.setSize(500,500);
			resultFrame.setLocation((int) (GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().getWidth() - 
					resultFrame.getWidth()), 50);
			
			
			
			
			
		}
	}
	
	
	/* saves the ExpResult object  in an lfr file.
	 * this method can be called by the save as menu item action
	 * listener on MainFrame */
	public void save() {
		JFileChooser saveChooser = new JFileChooser();
		saveChooser
		.setFileFilter(new FileNameExtensionFilter(
				"result (lfr) file","lfr"));
		if (saveChooser.showSaveDialog(containingFrame)==JFileChooser.APPROVE_OPTION){
			File selectedFile = saveChooser.getSelectedFile();

			selectedFile = selectedFile.getName().endsWith(".lfr")? selectedFile : new File(selectedFile+".lfr");
			if (selectedFile.exists()) 
				//if selected cancel
				if(JOptionPane.showConfirmDialog(containingFrame, "File exists. overwrite?", "File exists", JOptionPane.OK_CANCEL_OPTION)==JOptionPane.CANCEL_OPTION)
					return;
				else
					selectedFile.delete();
			try {
				selectedFile.createNewFile();
				//try with resources statement which makes this is a JRE1.7 compliant project
				try (
				OutputStream fileStream = new FileOutputStream(selectedFile);
				OutputStream bufferedStream = new BufferedOutputStream(fileStream);
				ObjectOutput objectOutput = new ObjectOutputStream(bufferedStream);
					)
						{
							objectOutput.writeObject(result);
						}
				catch (Exception e2) {		
					LOGGER.error("couldn't write to file", e2);
				}
	
			} catch (IOException e1) {
				LOGGER.error("couldn't create file", e1);
			}					
		}
	}
	
	/* saves the face of the ExpResult object in an lfc file.
	 * this method can be called by the save as menu item action
	 * listener on MainFrame */
	public void exportFace() {
		JFileChooser saveChooser = new JFileChooser();
		saveChooser
		.setFileFilter(new FileNameExtensionFilter(
				"calibration (lfc) file","lfc"));
		if (saveChooser.showSaveDialog(containingFrame)==JFileChooser.APPROVE_OPTION){
			File selectedFile = saveChooser.getSelectedFile();
			selectedFile = selectedFile.getName().endsWith(".lfc")? selectedFile : new File(selectedFile+".lfc");
			if (selectedFile.exists()) 
				//if selected cancel
				if(JOptionPane.showConfirmDialog(containingFrame, "File exists. overwrite?", "File exists", JOptionPane.OK_CANCEL_OPTION)==JOptionPane.CANCEL_OPTION)
					return;
				else
					selectedFile.delete();
			try {
				selectedFile.createNewFile();
				//try with resources statement which makes this is a JRE1.7 compliant project
				try (
				OutputStream fileStream = new FileOutputStream(selectedFile);
				OutputStream bufferedStream = new BufferedOutputStream(fileStream);
				ObjectOutput objectOutput = new ObjectOutputStream(bufferedStream);
					)
						{
							objectOutput.writeObject(result.getFace());
						}
				catch (Exception e2) {		
					LOGGER.error("couldn't write to file", e2);
				}
	
			} catch (IOException e1) {
				LOGGER.error("couldn't create file", e1);
			}					
		}
	}
	
}
