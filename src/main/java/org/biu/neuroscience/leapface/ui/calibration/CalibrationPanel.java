package org.biu.neuroscience.leapface.ui.calibration;

import java.awt.Color;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;

/* the jpanel for the calibration flow.
 * comprises the buttons for this flow and 3 canvases (not awt canvas,
 * but our own custon derived jpanels), layered one on top of each other,
 * the faceCanvas, the penCanvas, and the snapCurveCanvas. functionality
 * is explained in the controller for this class, CalibrationPanelController 
 */
public class CalibrationPanel extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2910548210801858591L;
	//buttons
	JToggleButton tglbtnInstructions;
	JButton btnInitializeCorners;
	JButton btnRec;
	JButton btnPause;
	JButton btnStop;
	JButton btnDone;
	
	//toolbar
	JToolBar toolBar;
	JToggleButton tglbtnPen;
	JToggleButton tglbtnInfoframe;
	
	
	//canvases and canvas properties
	float canvasAspectRatio;
	JLayeredPane canvasesContainer;
	CalibrationFaceCanvasPanel faceCanvas;
	CalibrationPenCanvasPanel penCanvas;
	CalibrationSnapCurveCanvasPanel snapCurveCanvas;
	JButton btnUndo;
	JButton btnDebugInit;
	
	
	public CalibrationPanel() {

		

		toolBar = new JToolBar();
		toolBar.setFloatable(false);
		toolBar.setOrientation(SwingConstants.VERTICAL);
		
		btnInitializeCorners = new JButton("Initialize Corners");
		btnInitializeCorners.setToolTipText("Initialize Corners");
		btnInitializeCorners.setIcon(new ImageIcon(CalibrationPanel.class.getResource("/icons/init.png")));
		tglbtnInstructions = new JToggleButton("Instructions");
		tglbtnInstructions.setToolTipText("Instructions");
		tglbtnInstructions.setIcon(new ImageIcon(CalibrationPanel.class.getResource("/icons/instructions.png")));

		btnRec = new JButton("Rec");
		btnRec.setToolTipText("Record");
		btnRec.setIcon(new ImageIcon(CalibrationPanel.class.getResource("/icons/rec.png")));
		btnPause = new JButton("Pause");
		btnPause.setToolTipText("Pause");
		btnPause.setIcon(new ImageIcon(CalibrationPanel.class.getResource("/icons/pause.png")));
		btnStop = new JButton("Stop");
		btnStop.setToolTipText("Stop");
		btnStop.setIcon(new ImageIcon(CalibrationPanel.class.getResource("/icons/stop.png")));
		btnDone = new JButton("Done");
		btnDone.setToolTipText("Done");
		btnDone.setIcon(new ImageIcon(CalibrationPanel.class.getResource("/icons/done.png")));
		canvasesContainer = new JLayeredPane();
		
		btnDebugInit = new JButton("Debug Init");
		btnDebugInit.setVisible(false);

		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(toolBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(btnDone)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(tglbtnInstructions)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnInitializeCorners)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnRec)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnPause)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnStop)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnDebugInit))
						.addComponent(canvasesContainer, GroupLayout.DEFAULT_SIZE, 672, Short.MAX_VALUE))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(24)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
						.addComponent(tglbtnInstructions, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(btnInitializeCorners, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(btnRec, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(btnPause, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(btnStop, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(btnDebugInit, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(toolBar, GroupLayout.PREFERRED_SIZE, 134, GroupLayout.PREFERRED_SIZE)
						.addComponent(canvasesContainer))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnDone)
					.addContainerGap())
		);
		
		
		canvasesContainer.setLayout(null);
		
		faceCanvas = new CalibrationFaceCanvasPanel(Color.BLACK);
		faceCanvas.setBounds(0, 0, 302, 586);
		canvasesContainer.add(faceCanvas);
		
		penCanvas = new CalibrationPenCanvasPanel(new Color(0,0,0,0), Color.RED);
		canvasesContainer.setLayer(penCanvas, 1);
		penCanvas.setBounds(0, 0, 302, 586);
		penCanvas.setBackground(new Color(0,0,0,0));
		penCanvas.setOpaque(false);
		canvasesContainer.add(penCanvas);

		snapCurveCanvas = new CalibrationSnapCurveCanvasPanel(new Color(0,255,255,200));
		canvasesContainer.setLayer(snapCurveCanvas, 2);
		snapCurveCanvas.setBounds(0, 0, 302, 586);
		snapCurveCanvas.setBackground(new Color(0,0,0,0));
		snapCurveCanvas.setOpaque(false);
		canvasesContainer.add(snapCurveCanvas);
		

		tglbtnInfoframe = new JToggleButton("");
		tglbtnInfoframe.setToolTipText("Information");
		toolBar.add(tglbtnInfoframe);
		tglbtnInfoframe.setIcon(new ImageIcon(CalibrationPanel.class.getResource("/icons/info.png")));

		tglbtnPen = new JToggleButton("");
		tglbtnPen.setToolTipText("Pen");
		tglbtnPen.setIcon(new ImageIcon(CalibrationPanel.class.getResource("/icons/pen.png")));
		toolBar.add(tglbtnPen);
		
		btnUndo = new JButton("");
		btnUndo.setToolTipText("Undo");
		toolBar.add(btnUndo);
		btnUndo.setIcon(new ImageIcon(CalibrationPanel.class.getResource("/icons/undo.png")));
		setLayout(groupLayout);
	}
}
