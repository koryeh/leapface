package org.biu.neuroscience.leapface.ui;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;


/* the panel for querying results.
 * contains three lines for different types of parametrised queries.
 * the first line is for total time spent on a face part.
 * the second line is to get all the time ranges in which some part was touched
 * the third line is to get all the blocks that are within a time range */
public class ResultPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -113764822284220956L;
	
	private static final int dropDownLength=15;
	
	JLabel lblExperminetLength;
	JLabel lblTotalSpentIn;
	JLabel lblPartRanges;
	JLabel lblRangeParts;
	JButton btnTotalSubmit;
	JButton btnPartRangesSubmit;
	JButton btnRangePartsSubmit;
	JComboBox<Integer> comboMin1;
	JComboBox<Integer> comboSec1;
	JLabel lblTo;
	JComboBox<Integer> comboMin2;
	JComboBox<Integer> comboSec2;
	JLabel lblColon1;
	JLabel lblColon2;
	JLabel lblExpLengthValue;
	JComboBox<String> comboPartRanges;
	JComboBox<String> comboTotal;
	public ResultPanel() {
		
		lblExperminetLength = new JLabel("Total experminet length: ");
		
		lblTotalSpentIn = new JLabel("Total time spent in: ");
		
		lblPartRanges = new JLabel("Time ranges spent in: ");
		
		lblRangeParts = new JLabel("Parts touched in time range from:");
		
		btnTotalSubmit = new JButton("Submit");
		btnTotalSubmit.setToolTipText("Submit");
		btnTotalSubmit.setIcon(new ImageIcon(ResultPanel.class.getResource("/icons/submit.png")));
		
		btnPartRangesSubmit = new JButton("Submit");
		btnPartRangesSubmit.setToolTipText("Submit");
		btnPartRangesSubmit.setIcon(new ImageIcon(ResultPanel.class.getResource("/icons/submit.png")));
		
		
		btnRangePartsSubmit = new JButton("Submit");
		btnRangePartsSubmit.setToolTipText("Submit");
		btnRangePartsSubmit.setIcon(new ImageIcon(ResultPanel.class.getResource("/icons/submit.png")));
		
		
		comboMin1 = new JComboBox<Integer>();
		
		comboSec1 = new JComboBox<Integer>();
		
		lblTo = new JLabel("to ");
		
		comboMin2 = new JComboBox<Integer>();
		
		comboSec2 = new JComboBox<Integer>();
		
		comboMin1.setMaximumRowCount(dropDownLength);
		comboMin2.setMaximumRowCount(dropDownLength);
		comboSec1.setMaximumRowCount(dropDownLength);
		comboSec2.setMaximumRowCount(dropDownLength);
		
		
		lblColon1 = new JLabel(":");
		
		lblColon2 = new JLabel(":");
		
		lblExpLengthValue = new JLabel("");
		
		comboPartRanges = new JComboBox<String>();
		
		comboTotal = new JComboBox<String>();
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblExperminetLength)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblExpLengthValue))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
								.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
									.addComponent(lblRangeParts)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(comboMin1, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
								.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(lblPartRanges)
										.addComponent(lblTotalSpentIn))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(comboPartRanges, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(comboTotal, 0, 102, Short.MAX_VALUE))))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblColon1)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(comboSec1, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblTo)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(comboMin2, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblColon2)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(comboSec2, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(btnTotalSubmit)
								.addComponent(btnPartRangesSubmit)
								.addComponent(btnRangePartsSubmit))))
					.addContainerGap(119, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblExperminetLength)
						.addComponent(lblExpLengthValue))
					.addGap(34)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblTotalSpentIn)
						.addComponent(btnTotalSubmit)
						.addComponent(comboTotal, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblPartRanges)
						.addComponent(btnPartRangesSubmit)
						.addComponent(comboPartRanges, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblRangeParts)
						.addComponent(comboMin1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(comboSec1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblTo)
						.addComponent(comboMin2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnRangePartsSubmit)
						.addComponent(comboSec2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblColon1)
						.addComponent(lblColon2))
					.addContainerGap(160, Short.MAX_VALUE))
		);
		setLayout(groupLayout);
	}
}
