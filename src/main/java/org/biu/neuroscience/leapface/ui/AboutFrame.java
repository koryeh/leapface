package org.biu.neuroscience.leapface.ui;

import java.awt.Color;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
 
/* Frame that's opened when help > about is hit on the menu*/
public class AboutFrame extends JFrame {

	private static final long serialVersionUID = -7377338816490411810L;
	JTextArea txtrHitInitialize;
	JLabel lblLogo;
	public AboutFrame() {
		setAlwaysOnTop(true);
		setTitle("About Leapface");
		txtrHitInitialize = new JTextArea();
		txtrHitInitialize.setWrapStyleWord(true);
		txtrHitInitialize.setText("\r\n\u2022 Version 0.9.0\r\n\u2022 Developed by Yehuda Koriat, Avi Ben Zaken\r\n\u2022 Contact: koryeh@gmail.com\r\n");
		txtrHitInitialize.setLineWrap(true);
		txtrHitInitialize.setFont(new Font("Courier New", Font.PLAIN, 16));
		txtrHitInitialize.setEditable(false);
		txtrHitInitialize.setColumns(10);
		txtrHitInitialize.setBackground(new Color(240,240,240,240));
		
		lblLogo = new JLabel("");
		lblLogo.setHorizontalAlignment(SwingConstants.CENTER);
		lblLogo.setIcon(new ImageIcon(AboutFrame.class.getResource("/logo.jpg")));
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(txtrHitInitialize)
						.addComponent(lblLogo, GroupLayout.PREFERRED_SIZE, 409, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblLogo, GroupLayout.PREFERRED_SIZE, 71, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(txtrHitInitialize, GroupLayout.PREFERRED_SIZE, 174, Short.MAX_VALUE))
		);
		getContentPane().setLayout(groupLayout);
	}
}
