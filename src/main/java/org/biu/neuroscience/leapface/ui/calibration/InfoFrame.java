package org.biu.neuroscience.leapface.ui.calibration;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JTextArea;

/* a jframe with a text area used to display
 * live data from the leap motion during the calibration process
 */
public class InfoFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5096476741761404780L;
	JTextArea text;
	public InfoFrame() {
		setAlwaysOnTop(true);
		
		text = new  JTextArea();
		text.setEditable(false);
		text.setColumns(10);
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(text, GroupLayout.DEFAULT_SIZE, 464, Short.MAX_VALUE)
					.addGap(0))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(text, GroupLayout.DEFAULT_SIZE, 262, Short.MAX_VALUE)
		);
		getContentPane().setLayout(groupLayout);
	}
	
	
	public void setText(String text) {
		this.text.setText(text);
		
	}

}
