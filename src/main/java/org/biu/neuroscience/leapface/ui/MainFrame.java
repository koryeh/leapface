package org.biu.neuroscience.leapface.ui;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import org.biu.neuroscience.leapface.ui.calibration.CalibrationPanel;


/* the main frame.
 * contains a menu, and 3 panels in cards layout,
 * the calibration panel, the experiment panel and the results panel. */
public class MainFrame  extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//menu
	JMenuBar menuBar;
	JMenu mnFile;
	JMenuItem mntmNewExperiment;
	JMenuItem mntmOpen;
	JMenuItem mntmSave;
	JMenuItem mntmExit;
	JMenu mnExperiment;
	JMenuItem mntmExportFace;
	JMenu mnHelp;
	JMenuItem mntmAbout;
	
	//panels
	LogoPanel logoPanel;
	CalibrationPanel calibPanel;
	ExperimentPanel expPanel;
	ResultPanel resPanel;
	JMenuItem mntmManual;
	
	
	public MainFrame() {

		setTitle("Leapface");
		setMinimumSize(new Dimension(1024,768));
		
		//menu bar
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		mntmNewExperiment = new JMenuItem("New experiment");
		mntmNewExperiment.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
		mnFile.add(mntmNewExperiment);
			
		mntmOpen = new JMenuItem("Open");
		mntmOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
		mnFile.add(mntmOpen);
		
		mntmSave = new JMenuItem("Save as");
		mntmSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
		mnFile.add(mntmSave);
		
		mntmExit = new JMenuItem("Exit");
		mntmExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_MASK));
		mnFile.add(mntmExit);
		
		mnExperiment = new JMenu("Experiment");
		menuBar.add(mnExperiment);
		
		mntmExportFace = new JMenuItem("Export face");
		mnExperiment.add(mntmExportFace);
		
		mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);
		
		mntmManual = new JMenuItem("Manual");
		mntmManual.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
		mnHelp.add(mntmManual);
		
		mntmAbout = new JMenuItem("About");
		mnHelp.add(mntmAbout);
		//end menu
		
		
		//layout
		getContentPane().setLayout(new CardLayout(0, 0));
		//end layout
		
		
		
		
		//panels
		LogoPanel logoPanel = new LogoPanel();
		getContentPane().add(logoPanel, "logoPanel");
		
		calibPanel = new CalibrationPanel();
		getContentPane().add(calibPanel, "calibPanel");
		
		expPanel = new ExperimentPanel();
		getContentPane().add(expPanel, "expPanel");
		
		resPanel = new ResultPanel();
		getContentPane().add(resPanel, "resPanel");
		//end panels
		
	}

	public void lockToFullscreen() {
		if (getExtendedState()!=MAXIMIZED_BOTH || !isUndecorated()) {
			setExtendedState(MAXIMIZED_BOTH);
			dispose();
			setUndecorated(true);
			setVisible(true);
		}
		
	}
	
	
	public void unlockFullscreen() {
		if (isUndecorated()) {
			dispose();
			setUndecorated(false);
			setVisible(true);
		}
	}


	
	
	
}
