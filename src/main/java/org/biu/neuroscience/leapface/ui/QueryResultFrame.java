package org.biu.neuroscience.leapface.ui;

import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JSplitPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;


/* A simple frame to display the results of queries */
public class QueryResultFrame extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7979853410503304039L;
	JSplitPane splitPane;
	JScrollPane scrollPane;
	JTextArea queryText;
	JTextPane resultText;

	public QueryResultFrame(String query, String result) {
		
		splitPane = new JSplitPane();
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(splitPane, GroupLayout.DEFAULT_SIZE, 434, Short.MAX_VALUE)
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(splitPane, GroupLayout.DEFAULT_SIZE, 262, Short.MAX_VALUE)
		);
		
		scrollPane = new JScrollPane();
		splitPane.setRightComponent(scrollPane);
		
		resultText = new JTextPane();
		scrollPane.setViewportView(resultText);
		
		queryText = new JTextArea();
		splitPane.setLeftComponent(queryText);
		getContentPane().setLayout(groupLayout);
		
		queryText.setText(query);
		
		resultText.setText(result);
		
		setTitle("Query result");
	}
}
