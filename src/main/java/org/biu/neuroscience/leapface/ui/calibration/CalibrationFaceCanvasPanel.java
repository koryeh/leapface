package org.biu.neuroscience.leapface.ui.calibration;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

/* This panel is a canvas on which the face, as it is captures from the controller, is rendered
 * basically it is just a jpanel that draws an (always changing) image when it's paintComponent 
 * method is called */
public class CalibrationFaceCanvasPanel extends JPanel implements CalibrationBrushableCanvasPanel  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8637860928349757780L;
	
	//initialised in CalibrationPanelController.InitializeCornersActionListener
	/*package*/ BufferedImage image;
	/*package*/ Color background;
	
	
	
	public CalibrationFaceCanvasPanel(Color background) {
		this.background=background;
		
	}
	
	public void init() {
		image=null;
		repaint();
	}

	public BufferedImage getImage() {
		return image;
	}

    
    public void paintComponent(Graphics g) {
        super.paintComponent(g);       
       g.drawImage(image,0,0,background,null);
      

    }  
}
