package org.biu.neuroscience.leapface.ui.calibration;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.Range;
import org.apache.commons.math3.geometry.euclidean.twod.Line;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.apache.commons.math3.ml.distance.EuclideanDistance;
import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.biu.neuroscience.leapface.experiment.FFinger;
import org.biu.neuroscience.leapface.experiment.FFrame;
import org.biu.neuroscience.leapface.experiment.Point;
import org.biu.neuroscience.leapface.experiment.Point2D;
import org.biu.neuroscience.leapface.experiment.face.Boundary;

/* Auxiliary methods for the calibration process*/
public class CalibrationUtils {

	/* takes a tape and decides what point it represents
	 * it takes the finger (by id) that appeared the longest and then
	 * averages the values for this finger */
	public static Point getCornerFromTape(List<FFrame> tape) {
		
		List<Double> xList = new ArrayList<Double>();
		List<Double> yList = new ArrayList<Double>();
		List<Double> zList = new ArrayList<Double>();
		
		
		Map<Integer,Integer> idCount = new HashMap<Integer,Integer>();
		for (FFrame frame : tape) {
			for (FFinger finger : frame.getFingers()) {
				Integer id = finger.getId();
				Integer currentCount = idCount.get(id);
				if (currentCount==null)	
					idCount.put(id, 1);
				else
					idCount.put(id, currentCount+1);
			}
		}
		Integer maxCount=0;
		Integer maxId =0;
		for (Map.Entry<Integer,Integer> entry : idCount.entrySet()) {
			if (entry.getValue()>maxCount) {
				maxId=entry.getKey();
				maxCount=entry.getValue();
			}
		}
		
		for (FFrame frame : tape) {
			for (FFinger finger : frame.getFingers()) {
				if (finger.getId().equals(maxId)) {
					xList.add(new Double(finger.getTip().getX()));
					zList.add(new Double(finger.getTip().getZ()));
					yList.add(new Double(finger.getTip().getY()));
				}
			}
		}
		Mean mean = new Mean();
		Double x = mean.evaluate(ArrayUtils.toPrimitive(xList.toArray(new Double[xList.size()])));
		Double y = mean.evaluate(ArrayUtils.toPrimitive(yList.toArray(new Double[yList.size()])));
		Double z = mean.evaluate(ArrayUtils.toPrimitive(zList.toArray(new Double[zList.size()])));

		return(new Point(new Float(x),new Float(y),new Float(z)));
	}
	
	/* brush a CalibrationBrushableCanvasPanel at a point with a square with 
	 * a side of brushSize	 */
	public static void brush(CalibrationBrushableCanvasPanel canvas, int x, int y, int r, int g, int b, int a, int brushSize) {
		
		int squareSize=brushSize*brushSize;
		
		int square[]= new int[4*squareSize];
		
		for (int i = 0; i<squareSize;i++) {
			square[4*i]=r;
			square[4*i+1]=g;
			square[4*i+2]=b;
			square[4*i+3]=a;
		}
		if (x>0 && y>0 && (x+brushSize)<canvas.getWidth() && (y+brushSize)<canvas.getHeight()) {
			canvas.getImage().getRaster().setPixels(x,y,brushSize,brushSize,square);
			canvas.repaint(x,y,brushSize,brushSize);
		}		
	}
	/* same thing but with a color instead of four ints	 */
	public static void brush (CalibrationBrushableCanvasPanel canvas, int x, int y, Color color, int brushSize) {
		brush(canvas,x,y,color.getRed(),color.getGreen(),color.getBlue(),color.getAlpha(),brushSize);
	}
	
	/* reduce curve density - thins out the curve so that every vertex is at least
	 * minimalVerticesDistance pixels away from the last boundary
	 * 
	 */
	public static List<int []> reduceCurveDensity(List<int []> curve, int minimalVerticesDistance) {
		
		List<int[]> newCurve = new ArrayList<int[]>(curve);
		
		EuclideanDistance distCalc = new EuclideanDistance();
		int [] previous =null;
		
		for (int i=0;i<newCurve.size() ;) {
			int[] current= newCurve.get(i);
			if (previous!=null) {
				
				double dist = distCalc.compute(new double[]{previous[0],previous[1]},new double[]{current[0],current[1]});
				if (dist<minimalVerticesDistance) {
					newCurve.remove(i);
					
				}
				//distance is spacious enough
				else {
					previous=current;
					i++;
				}
			}
			//previous==null
			else {
				previous=current;
				i++;
			}
				
		}
		return newCurve;
	}
	
	
	
	/* create a boundary object from curve.
	 * first, the density is reduces using the methods above.
	 * then we do the relative distance from edge trick again (as we did when 
	 * we rendered the face), just the other way around	 */
	public static Boundary curveToBoundary(List<int []> curve, int minimalVerticesDistance, int canvasWidth,
			int canvasHeight, float recWidth,float recHeight,float leftEdge, float topEdge) {
		
		//reduce curve density
		curve = CalibrationUtils.reduceCurveDensity(curve,minimalVerticesDistance);
		
		//translate pixel coordinates to leap coordinates 
		ArrayList<Point2D> boundaryList = new ArrayList<Point2D>();
		for (int [] pixelCo : curve) {
			float relativeX = (float)pixelCo[0]/(float)canvasWidth;
			float relativeZ = (float)pixelCo[1]/(float)canvasHeight;
			float x = relativeX*recWidth+leftEdge;
			float z = topEdge-relativeZ*recHeight;
			Point2D translatedPoint = new Point2D(x,z);
			boundaryList.add(translatedPoint);
		}
		 
		return new Boundary(boundaryList);
	}
	
	
	
	/* curves are treated as polygons. we are looking to detect self intersecting
	 * polygons, because those are bad news.
	 * since the dataset is relatively small this is a naive O(n^2) algorithm.
	 * we check every edge pair to see if the intersection point of the lines
	 * they create is within their bounds */
	public static boolean isCurveSelfIntersecting(List<int[/*2*/]> curve) {
		List<int[/*4*/]> edges = new ArrayList<int[]>();
		
		//create edges, each edge is an 4 array of ints, two first members are first vertex etc
		int [] previous =null;
		
		for (int[] current : curve ) {
			if (previous!=null) {
				int[] newEdge = {previous[0],previous[1],current[0],current[1]};
				edges.add(newEdge);
			}
			previous=current;
		}
		
		
		for (int checkedIndex=0; checkedIndex < edges.size();checkedIndex++) {
			for (int checkedAgainstIndex=0; checkedAgainstIndex < edges.size();checkedAgainstIndex++) {
				//if edge each checked against itself or one of it's neighbours, continue
				if (Math.abs(checkedIndex-checkedAgainstIndex)<2)
					continue;
				int [] checked=edges.get(checkedIndex);
				int [] checkedAgainst=edges.get(checkedAgainstIndex);


				
				Vector2D checkedFirstVertex = new Vector2D(checked[0], checked[1]);
				Vector2D checkedSecondVertex = new Vector2D(checked[2], checked[3]);
				Vector2D checkedAgainstFirstVertex = new Vector2D(checkedAgainst[0], checkedAgainst[1]);
				Vector2D checkedAgainstSecondVertex = new Vector2D(checkedAgainst[2], checkedAgainst[3]);
				Line checkedEdgeLine = new Line(checkedFirstVertex,checkedSecondVertex);
				Line checkedAgainstEdgeLine = new Line(checkedAgainstFirstVertex,checkedAgainstSecondVertex);
				Vector2D intersection = checkedEdgeLine.intersection(checkedAgainstEdgeLine);
				
				if (intersection==null) {
					continue;
				}
					
				//check if intersection is withing the segments
				
				
				/* There was a nasty bug due to floating point approximation errors,
				 * so the number had to be truncated to something reasonable */
				double interX = (double) Math.round(intersection.getX()*1000)/1000;
				/*apaches vector2d calls the dimensions x and y, we use x and z
				 * because as far as leap is concerned y is that height dimension */
				double interZ = (double) Math.round(intersection.getY()*1000)/1000;
				Range<Double> checkedXRange = Range.between((double) checked[0],(double) checked[2]);
				Range<Double> checkedZRange = Range.between((double) checked[1],(double) checked[3]);
				Range<Double> checkedAgainstXRange = Range.between((double) checkedAgainst[0],(double) checkedAgainst[2]);
				Range<Double> checkedAgainstZRange = Range.between((double) checkedAgainst[1],(double) checkedAgainst[3]);
	

				if (checkedXRange.contains(interX) && checkedZRange.contains(interZ) &&
						checkedAgainstXRange.contains(interX) && checkedAgainstZRange.contains(interZ)) {
					return true;
				}

				
			}
			
		}
		return false;
		
		
		
	}
	
}
