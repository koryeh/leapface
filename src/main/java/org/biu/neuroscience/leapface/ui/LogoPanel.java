package org.biu.neuroscience.leapface.ui;

import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/* A panel displaying the logo, the first panel displayed on frame */
public class LogoPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3623904240466962382L;

	public LogoPanel() {
		setLayout(new GridLayout(1, 0, 0, 0));
		
		JLabel lblLogo = new JLabel("");
		lblLogo.setIcon(new ImageIcon(LogoPanel.class.getResource("/logo.jpg")));
		lblLogo.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblLogo);
	}

}
